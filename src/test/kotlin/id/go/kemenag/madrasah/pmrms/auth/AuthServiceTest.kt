package id.go.kemenag.madrasah.pmrms.auth

import id.go.kemenag.madrasah.pmrms.auth.model.request.auth.ForgotPasswordRequest
import id.go.kemenag.madrasah.pmrms.auth.model.request.auth.LoginRequest
import id.go.kemenag.madrasah.pmrms.auth.model.request.auth.ResetPasswordRequest
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.service.AuthService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

@Suppress("UNCHECKED_CAST")
@TestMethodOrder(MethodOrderer.MethodName::class)
@SpringBootTest
class AuthServiceTest {

    @Autowired
    private lateinit var service: AuthService

    companion object {
        var forgotPasswordRequestId = ""
    }

    @Test
    @DisplayName("Test Login")
    fun test1() {
        println("Test Login")
        val test: ResponseEntity<ReturnData> = service.login(LoginRequest("ptm15@madrasah.kemenag.go.id", "User@madrasah2022"))
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @Test
    @DisplayName("Test Detail")
    fun test2() {
        println("Test Detail")
        val test: ResponseEntity<ReturnData> = service.detail()
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @Test
    @DisplayName("Test Forgot Password")
    fun test3() {
        println("Test Forgot Password")
        val test: ResponseEntity<ReturnData> =
            service.forgotPassword(ForgotPasswordRequest("ptm15@madrasah.kemenag.go.id"), true)
        val returnData: ReturnData? = test.body
        val responseData: Map<String, Any> = returnData?.data as Map<String, Any>
        forgotPasswordRequestId = responseData["requestId"].toString()

        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @Test
    @DisplayName("Test Reset Password")
    fun test4() {
        println("Test Reset Password")
        val test: ResponseEntity<ReturnData> =
            service.resetPassword(
                ResetPasswordRequest(
                    forgotPasswordRequestId,
                    "User@madrasah2022",
                    "User@madrasah2022"
                )
            )
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }
}
