package id.go.kemenag.madrasah.pmrms.auth.model.response

import id.go.kemenag.madrasah.pmrms.auth.pojo.Files


data class ReturnDataFiles(
    var success: Boolean? = false,
    var data: Files? = null,
    var message: String? = null
)
