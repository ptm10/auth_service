package id.go.kemenag.madrasah.pmrms.auth.controller

import id.go.kemenag.madrasah.pmrms.auth.helpers.responseBadRequest
import id.go.kemenag.madrasah.pmrms.auth.helpers.responseNotFound
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import io.swagger.annotations.Api
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Errors"], description = "Errors API")
@RestController
@RequestMapping(path = ["errors"])
class ErrorsContrroller {

    @PostMapping(value = ["{code}"], produces = ["application/json"])
    fun post(@PathVariable code: String): ResponseEntity<ReturnData> {
        try {
            if (code == "400") {
                return responseBadRequest()
            }

            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }
}
