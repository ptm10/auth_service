package id.go.kemenag.madrasah.pmrms.auth.constant

const val TASK_TYPE_CHANGE_ROLE_REQUEST = 1

const val TASK_STATUS_NEW = 0

const val TASK_STATUS_ON_PROGRESS = 1

const val TASK_STATUS_APPROVED = 2

const val TASK_STATUS_REJECT = 3
