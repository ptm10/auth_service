package id.go.kemenag.madrasah.pmrms.auth.model.request.approval


import id.go.kemenag.madrasah.pmrms.auth.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

class ApproveRequest(

    @field:NotEmpty(message = "Approval id $VALIDATOR_MSG_REQUIRED")
    var approvalId: String? = null,

    @field:NotNull(message = "Type $VALIDATOR_MSG_REQUIRED")
    var type: Int? = null,

    @field:NotNull(message = "Status $VALIDATOR_MSG_REQUIRED")
    var approveStatus: Int? = null,

    var rejectedMessage: String? = null
)
