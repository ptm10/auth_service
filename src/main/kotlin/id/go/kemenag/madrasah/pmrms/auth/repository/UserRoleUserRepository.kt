package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.UsersRoleUser
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface UserRoleUserRepository : JpaRepository<UsersRoleUser, String> {

    @Query("select ur.userId from UsersRoleUser ur where ur.roleId in(:rolesId) and ur.active = true and ur.user.componentId = :componentId GROUP BY ur.userId having count(distinct ur.roleId) = :length")
    fun getUserIdsByRoleIdsComponentId(rolesId: List<String>, length: Long, componentId: String?): List<String>

    @Query("select ur.userId from UsersRoleUser ur where ur.roleId in(:rolesId) and ur.active = true GROUP BY ur.userId having count(distinct ur.roleId) = :length")
    fun getUserIdsByRoleIds(rolesId: List<String>, length: Long): List<String>
}
