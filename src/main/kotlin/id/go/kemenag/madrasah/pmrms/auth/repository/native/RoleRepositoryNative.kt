package id.go.kemenag.madrasah.pmrms.auth.repository.native

import id.go.kemenag.madrasah.pmrms.auth.pojo.Role
import org.springframework.stereotype.Repository

@Repository
class RoleRepositoryNative : BaseRepositoryNative<Role>(Role::class.java)
