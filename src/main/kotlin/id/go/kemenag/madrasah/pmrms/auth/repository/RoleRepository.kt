package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.Role
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface RoleRepository : JpaRepository<Role, String> {

    @Query(
        "select r from Role r " +
                "WHERE r.active = true " +
                "AND (LOWER(r.name) LIKE %:param%)"
    )
    fun getListPagination(@Param("param") param: String?, pageable: Pageable?): Page<Role?>?

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<Role>

    fun getAllByActive(active: Boolean = true): List<Role>
}
