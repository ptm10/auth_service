package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.UpdateRoleRequest
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UpdateRoleRequestRepository : JpaRepository<UpdateRoleRequest, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<UpdateRoleRequest>

    fun findByUserIdAndApproveStatusAndActive(
        userId: String,
        approveStatus: Int,
        active: Boolean = true
    ): Optional<UpdateRoleRequest>
}
