package id.go.kemenag.madrasah.pmrms.auth.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "resources", schema = "public")
data class UsersResources(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "phone_number")
    var phoneNumber: String? = null,

    @Column(name = "position_id")
    var positionId: String? = null,

    @ManyToOne
    @JoinColumn(name = "position_id", insertable = false, updatable = false, nullable = true)
    @Where(clause = "active = true")
    var position: Position? = null,

    @Column(name = "supervisior_id")
    var supervisiorId: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
