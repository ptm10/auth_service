package id.go.kemenag.madrasah.pmrms.auth.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.auth.constant.HEADER_STRING
import id.go.kemenag.madrasah.pmrms.auth.model.request.DeleteDataRequest
import id.go.kemenag.madrasah.pmrms.auth.model.request.notif.EmailRequest
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnDataFiles
import kong.unirest.Unirest
import org.apache.http.client.HttpClient
import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.conn.ssl.TrustSelfSignedStrategy
import org.apache.http.impl.client.HttpClients
import org.apache.http.ssl.SSLContextBuilder
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext

class RequestHelpers {

    companion object {
        private fun configHttpClient() {
            val sslContext: SSLContext = SSLContextBuilder().loadTrustMaterial(
                null,
                object : TrustSelfSignedStrategy() {
                    override fun isTrusted(chain: Array<X509Certificate?>?, authType: String?): Boolean {
                        return true
                    }
                }
            ).build()

            val customHttpClient: HttpClient = HttpClients.custom().setSSLContext(sslContext)
                .setSSLHostnameVerifier(NoopHostnameVerifier()).build()

            Unirest.config().httpClient(customHttpClient)
        }

        fun notifSendEmail(url: String, email: EmailRequest, bearer: String = ""): ReturnData? {
            return try {
                configHttpClient()
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val reqUrl = "$url/email/send"

                val response = Unirest.post(reqUrl)
                    .header("Content-Type", "application/json")
                    .header(HEADER_STRING, bearer)
                    .body(ObjectMapper().writeValueAsString(email))
                    .asString()

                objectMapper.readValue(response.body, ReturnData::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

        fun filesGetById(id: String, baseUrl: String, bearer: String): ReturnData {
            val rData = ReturnData()
            rData.message = "Gagal melakukan request File"
            try {
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val reqUrl = "$baseUrl/files/get-by-id?id=$id"
                val response = Unirest.get(reqUrl)
                    .header("Content-Type", "application/json")
                    .header(HEADER_STRING, bearer)
                    .asString()

                val responseData: ReturnDataFiles? = objectMapper.readValue(response.body, ReturnDataFiles::class.java)

                responseData?.let {
                    it.success?.let { s ->
                        if (s) {
                            rData.success = true
                            rData.message = ""
                            rData.data = it.data
                        } else {
                            if (!it.message.isNullOrEmpty()) {
                                rData.message = it.message
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return rData
        }

        fun filesDelete(id: String, baseUrl: String, bearer: String): ReturnData? {
            return try {
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val response = Unirest.delete("$baseUrl/files")
                    .header("Content-Type", "application/json")
                    .header(HEADER_STRING, bearer)
                    .body(DeleteDataRequest(id))
                    .asString()

                objectMapper.readValue(response.body, ReturnData::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

        fun requestTableauToken(tableauTokenUrl: String): String? {
            return try {
                val response = Unirest.post(tableauTokenUrl)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .body("username=user_public")
                    .asString()

                return response.body
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
    }
}
