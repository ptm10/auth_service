package id.go.kemenag.madrasah.pmrms.auth.repository

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UnitRepository : JpaRepository<id.go.kemenag.madrasah.pmrms.auth.pojo.Unit, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<id.go.kemenag.madrasah.pmrms.auth.pojo.Unit>
}
