package id.go.kemenag.madrasah.pmrms.auth.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "log_activity", schema = "auth")
data class LogActivity(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "user_id")
    var userId: String? = null,

    @Column(name = "ip_address")
    var ipAddress: String? = null,

    @Column(name = "data_source")
    var dataSource: String? = null,

    @Column(name = "change_data")
    var changeData: String? = null,

    @Column(name = "old_data")
    var oldData: String? = null,

    @Column(name = "new_data")
    var newData: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date()
)
