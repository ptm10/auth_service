package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.UsersRole
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface UserRoleRepository : JpaRepository<UsersRole, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<UsersRole>

    fun findByUserIdAndRoleIdAndActive(userId: String?, roleId: String?, active: Boolean = true): Optional<UsersRole>

    fun findByRoleIdInAndActive(roleId: List<String>?, active: Boolean = true): List<UsersRole>

    @Query(
        "select ur.userId from UsersRole ur " +
                "where ur.active = true " +
                "and ur.roleId in(:roleIds)"
    )
    fun selectUserIdByRoleInAndActive(@Param("roleIds") roleIds: List<String>?): List<String>
}
