package id.go.kemenag.madrasah.pmrms.auth.repository.native

import id.go.kemenag.madrasah.pmrms.auth.pojo.Users
import org.springframework.stereotype.Repository

@Repository
class UserRepositoryNative : BaseRepositoryNative<Users>(Users::class.java)
