package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.LogActivity
import org.springframework.data.jpa.repository.JpaRepository

interface LogActivityRepository : JpaRepository<LogActivity, String> {

}
