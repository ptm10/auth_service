package id.go.kemenag.madrasah.pmrms.auth.exception

import org.springframework.http.HttpStatus

/**
 * The 415 (Unsupported Media Type) status code indicates that the
 * origin server is refusing to service the request because the payload
 * is in a format not supported by this method on the target resource.
 * The format problem might be due to the request's indicated
 * Content-Type or Content-Encoding, or as a result of inspecting the
 * data directly.
 */
class UnsupportedMediaTypeException : BaseException {

    constructor() {
        this.code = "415"
        this.message = "Unsupported Media Type"
        this.status = HttpStatus.UNSUPPORTED_MEDIA_TYPE
    }

    constructor(message: String?) {
        this.code = "415"
        this.message = message!!
        this.status = HttpStatus.UNSUPPORTED_MEDIA_TYPE
    }

}
