package id.go.kemenag.madrasah.pmrms.auth.constant

const val EXPIRATION_TIME: Long = 864000000 // 10 days (1 hour = 3600000)
const val SECRET = "PMRMS-2022"
const val TOKEN_PREFIX = "Bearer "
const val HEADER_STRING = "Authorization"

val USER_ADMIN_ALLOWED_PATH =
    listOf(
        "/auth/logout",
        "/auth/detail",
        "/auth/update-profile",
        "/auth/update-password",
        "/auth/roles",
        "/approval/*",
        "/user/*",
        "/auth/required-change-password",
        "/auth/cancel-update-role-request",
        "/auth/change-email",
        "/auth/change-email-request",
        "/auth/cancel-update-email",
        "/role/*",
        "/unit/*",
        "/position/*"
    )

val AUDIENCE_FILTER_PATH = mapOf(
    "user-admin" to USER_ADMIN_ALLOWED_PATH
)

val PASSWORD_REGEX = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+\\-=\\[\\]{};':\"|,.<>\\/?])(?=\\S+$).{8,}"
val EMAIL_VALID_REGEX = "@madrasah.kemenag.go.id"
