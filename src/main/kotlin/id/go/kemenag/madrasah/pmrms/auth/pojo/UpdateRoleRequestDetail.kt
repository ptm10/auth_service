package id.go.kemenag.madrasah.pmrms.auth.pojo

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "update_role_request_detail", schema = "auth")
data class UpdateRoleRequestDetail(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "update_role_request_id")
    var updateRoleRequestId: String? = null,

    @Column(name = "role_id")
    var roleId: String? = null,

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false, nullable = true)
    var role: Role? = null,

    @Column(name = "request_status")
    var requestStatus: Int? = null
)
