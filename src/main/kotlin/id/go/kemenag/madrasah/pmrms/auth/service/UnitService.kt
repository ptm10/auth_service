package id.go.kemenag.madrasah.pmrms.auth.service

import id.go.kemenag.madrasah.pmrms.auth.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.auth.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.repository.native.UnitRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Suppress("UNCHECKED_CAST")
@Service
class UnitService {

    @Autowired
    private lateinit var repoNative: UnitRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}
