package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.VUsersResources
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface VUserResourcesRepository : JpaRepository<VUsersResources, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<VUsersResources>
}
