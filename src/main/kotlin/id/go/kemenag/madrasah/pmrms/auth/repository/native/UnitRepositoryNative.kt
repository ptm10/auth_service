package id.go.kemenag.madrasah.pmrms.auth.repository.native

import org.springframework.stereotype.Repository

@Repository
class UnitRepositoryNative : BaseRepositoryNative<id.go.kemenag.madrasah.pmrms.auth.pojo.Unit>(id.go.kemenag.madrasah.pmrms.auth.pojo.Unit::class.java)
