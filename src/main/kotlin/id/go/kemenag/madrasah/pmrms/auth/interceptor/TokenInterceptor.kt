package id.go.kemenag.madrasah.pmrms.auth.interceptor

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTCreationException
import com.auth0.jwt.exceptions.JWTDecodeException
import com.auth0.jwt.exceptions.JWTVerificationException
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.auth.constant.*
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.pojo.VUsersResources
import id.go.kemenag.madrasah.pmrms.auth.repository.VUserResourcesRepository
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.context.support.WebApplicationContextUtils
import org.springframework.web.util.ContentCachingRequestWrapper
import java.util.*
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class TokenInterceptor : Filter {

    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {
        val req = ContentCachingRequestWrapper(request as HttpServletRequest)
        val res = response as HttpServletResponse

        // Read the Authorization header, where the JWT token should be
        val header = req.getHeader(HEADER_STRING)

        // If header doesn't contain Bearer or is null will return nothing and exit
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            responseException(res, "Token $VALIDATOR_MSG_REQUIRED")
            return
        }


        var authentication: UsernamePasswordAuthenticationToken? = null
        var successAuth = true

        try {
            authentication = getAuthentication(req)
        } catch (e: JWTDecodeException) {
            successAuth = false
            responseException(res, e.message.toString())
        } catch (e: JWTCreationException) {
            successAuth = false
            responseException(res, e.message.toString())
        } catch (e: JWTVerificationException) {
            successAuth = false
            responseException(res, e.message.toString())
        } catch (e: IllegalArgumentException) {
            successAuth = false
            responseException(res, e.message.toString())
        } catch (e: Exception) {
            successAuth = false
            responseException(res, e.message.toString())
        }

        SecurityContextHolder.getContext().authentication = authentication
        if (successAuth) {
            chain?.doFilter(request, response)
        }
    }

    private fun responseException(response: HttpServletResponse, message: String) {
        val json = ObjectMapper().writeValueAsString(
            ReturnData(
                message = message
            )
        )

        response.contentType = "application/json"
        response.status = HttpServletResponse.SC_UNAUTHORIZED
        response.writer.write(json)
        response.writer.flush()
        response.writer.close()
    }

    @Throws(
        JWTDecodeException::class,
        JWTCreationException::class,
        JWTVerificationException::class,
        IllegalArgumentException::class
    )
    private fun getAuthentication(request: HttpServletRequest): UsernamePasswordAuthenticationToken {
        val token = request.getHeader(HEADER_STRING)
        // parse jwt token from request and validate it internal authentication
        val jwt =
            JWT.require(Algorithm.HMAC512(SECRET.toByteArray())).build().verify(token.replace(TOKEN_PREFIX, ""))
        return if (jwt.subject != null) { // return principal username
            val audience = jwt.audience?.get(0)
            var isAllowed = false

            AUDIENCE_FILTER_PATH[audience]?.forEach lambda@{
                if (it.contains("/*")) {
                    if (request.requestURI.contains(it.replace("/*", ""))) {
                        isAllowed = true
                        return@lambda
                    }
                } else {
                    if (request.requestURI == it) {
                        isAllowed = true
                        return@lambda
                    }
                }
            }

            if (isAllowed) {
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                val users: VUsersResources = objectMapper.readValue(jwt.subject, VUsersResources::class.java)

                val servletContext = request.session.servletContext
                val webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext)
                val repoUser = webApplicationContext!!.getBean(VUserResourcesRepository::class.java)

                val checkUser = repoUser.findByIdAndActive(users.id)

                if (checkUser.isPresent) {
                    val findRoleLsp = checkUser.get().roles?.find { f ->
                        f.roleId == ROLE_ID_LSP
                    }

                    if (findRoleLsp != null) {
                        if (Date().before(checkUser.get().lsp?.startDate)) {
                            throw Exception("Tanggal LSP User Belum di Mulai")
                        }
                        if (Date().after(checkUser.get().lsp?.endDate)) {
                            throw Exception("Tanggal LSP User Sudah Berakhir")
                        }
                    }

                    UsernamePasswordAuthenticationToken(
                        objectMapper.writeValueAsString(checkUser.get()),
                        audience,
                        ArrayList()
                    )
                } else {
                    throw Exception("User $VALIDATOR_MSG_NOT_FOUND")
                }
            } else {
                throw Exception("User $VALIDATOR_MSG_NOT_HAVE_ACCESS")
            }
        } else {
            throw Exception("Token $VALIDATOR_MSG_NOT_VALID")
        }
    }
}
