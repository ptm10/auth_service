package id.go.kemenag.madrasah.pmrms.auth.model.request.auth


import id.go.kemenag.madrasah.pmrms.auth.constant.VALIDATOR_MSG_REQUIRED
import id.go.kemenag.madrasah.pmrms.auth.validator.EmailValidator
import javax.validation.constraints.NotEmpty

data class ChangeEmailRequestRequestRequest(

    @field:NotEmpty(message = "Email $VALIDATOR_MSG_REQUIRED")
    @field:EmailValidator
    var email: String? = null,
)
