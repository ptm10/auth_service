package id.go.kemenag.madrasah.pmrms.auth.model.request

class Pagination2Request(

    var enablePage: Boolean? = true,

    var page: Int? = 0,

    var size: Int? = 10,

    var sort: MutableList<Sort>? = emptyList<Sort>().toMutableList(),

    var paramLike: MutableList<ParamSearch>? = emptyList<ParamSearch>().toMutableList(),

    var paramIs: MutableList<ParamSearch>? = emptyList<ParamSearch>().toMutableList(),

    var paramIn: MutableList<ParamArray>? = emptyList<ParamArray>().toMutableList(),

    var paramNotIn: MutableList<ParamArray>? = emptyList<ParamArray>().toMutableList()
)

class Sort(
    var field: String? = null,
    var direction: String? = null
)

class ParamSearch(
    var field: String? = null,
    var dataType: String? = null,
    var value: String? = null
)

class ParamArray(
    var field: String? = null,
    var dataType: String? = null,
    var value: List<String>? = null
)
