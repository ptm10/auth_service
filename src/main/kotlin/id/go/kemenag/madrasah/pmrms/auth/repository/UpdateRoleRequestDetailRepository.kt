package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.UpdateRoleRequestDetail
import org.springframework.data.jpa.repository.JpaRepository

interface UpdateRoleRequestDetailRepository : JpaRepository<UpdateRoleRequestDetail, String> {
}
