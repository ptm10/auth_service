package id.go.kemenag.madrasah.pmrms.auth.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users_required_change_password", schema = "auth")
data class UsersRequiredChangePassword(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "user_id")
    var userId: String? = null,

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false, nullable = true)
    var user: Users? = null,

    @Column(name = "changed")
    var changed: Boolean? = false,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date()
)
