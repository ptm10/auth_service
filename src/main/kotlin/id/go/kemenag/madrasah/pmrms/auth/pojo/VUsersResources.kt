package id.go.kemenag.madrasah.pmrms.auth.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "v_user_resources", schema = "auth")
data class VUsersResources(
    @Id
    @Column(name = "id")
    var id: String? = null,

    @Column(name = "first_name")
    var firstName: String? = null,

    @Column(name = "last_name")
    var lastName: String? = null,

    @Column(name = "email")
    var email: String? = null,

    @Column(name = "password")
    @JsonIgnore
    var password: String? = null,

    @Column(name = "profile_picture_id")
    var profilePictureId: String? = null,

    @ManyToOne
    @JoinColumn(name = "profile_picture_id", insertable = false, updatable = false, nullable = true)
    var profilePicture: Files? = null,

    @Column(name = "component_id")
    var componentId: String? = null,

    @ManyToOne
    @JoinColumn(name = "component_id", insertable = false, updatable = false, nullable = true)
    @Where(clause = "active = true")
    var component: Component? = null,

    @Column(name = "province_id")
    var provinceId: String? = null,

    @ManyToOne
    @JoinColumn(name = "province_id", insertable = false, updatable = false, nullable = true)
    @Where(clause = "active = true")
    var province: Province? = null,

    @OneToMany(mappedBy = "userId")
    @Where(clause = "active = true")
    var roles: MutableSet<UsersRole>? = null,

    @Column(name = "resources_id")
    var resourcesId: String? = null,

    @ManyToOne
    @JoinColumn(name = "resources_id", insertable = false, updatable = false, nullable = true)
    @Where(clause = "active = true")
    var resources: UsersResources? = null,

    @Column(name = "lsp_id")
    var lspId: String? = null,

    @ManyToOne
    @JoinColumn(name = "lsp_id", insertable = false, updatable = false, nullable = true)
    @Where(clause = "active = true")
    var lsp: LspDate? = null,

    @Column(name = "last_login")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var lastLogin: Date? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
