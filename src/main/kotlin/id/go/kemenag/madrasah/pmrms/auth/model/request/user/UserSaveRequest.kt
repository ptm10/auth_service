package id.go.kemenag.madrasah.pmrms.auth.model.request.user

import id.go.kemenag.madrasah.pmrms.auth.constant.VALIDATOR_MSG_REQUIRED
import id.go.kemenag.madrasah.pmrms.auth.validator.EmailValidator
import javax.validation.constraints.NotEmpty

class UserSaveRequest(

    @field:NotEmpty(message = "Email $VALIDATOR_MSG_REQUIRED")
    @field:EmailValidator
    var email: String? = null,

    @field:NotEmpty(message = "Nama Depan $VALIDATOR_MSG_REQUIRED")
    var firstName: String? = null,

    var lastName: String? = null,

    var profilePictureId: String? = null,

    var componentId: String? = null,

    var provinceId: String? = null,

    @field:NotEmpty(message = "Role $VALIDATOR_MSG_REQUIRED")
    var roles: List<String>? = null,

    var lspStartDate: String? = null,

    var lspEndDate: String? = null,

    var lspAwpImplementationIds: List<String>? = null
)
