package id.go.kemenag.madrasah.pmrms.auth.model.request.auth


import id.go.kemenag.madrasah.pmrms.auth.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class UpdateProfileRequest(

    @field:NotEmpty(message = "Nama Depan $VALIDATOR_MSG_REQUIRED")
    var firstName: String? = null,

    @field:NotEmpty(message = "Nama Belakang $VALIDATOR_MSG_REQUIRED")
    var lastName: String? = null,

    var profilePictureId: String? = null,

    var roleRequest: List<RoleRequest>? = null,

    var email: String? = null
)


data class RoleRequest(

    var roleId: String? = null,

    var status: Int? = null
)
