package id.go.kemenag.madrasah.pmrms.auth.repository


import id.go.kemenag.madrasah.pmrms.auth.pojo.Users
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserRepository : JpaRepository<Users, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<Users>

    fun findByEmailAndActive(email: String?, active: Boolean = true): Optional<Users>

    fun getByIdInAndProvinceIdAndActive(
        ids: List<String>,
        provinceId: String?,
        active: Boolean = true
    ): List<Users>
}
