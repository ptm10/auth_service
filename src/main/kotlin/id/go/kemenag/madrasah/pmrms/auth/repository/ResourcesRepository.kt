package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.Resources
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ResourcesRepository : JpaRepository<Resources, String> {

    fun findByUserIdAndActive(userId: String?, active: Boolean = true): Optional<Resources>
}
