package id.go.kemenag.madrasah.pmrms.auth.exception

import org.springframework.http.HttpStatus

/**
 * The 404 (Not Found) status code indicates that the origin server did
 * not find a current representation for the target resource or is not
 * willing to disclose that one exists.  A 404 status code does not
 * indicate whether this lack of representation is temporary or
 * permanent.
 */
class NotFoundException : BaseException {

    constructor() {
        this.code = "404"
        this.message = "Not Found"
        this.status = HttpStatus.NOT_FOUND
    }

    constructor(message: String?) {
        this.code = "404"
        this.message = message!!
        this.status = HttpStatus.NOT_FOUND
    }

}
