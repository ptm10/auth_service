package id.go.kemenag.madrasah.pmrms.auth.repository.native

import id.go.kemenag.madrasah.pmrms.auth.pojo.UsersResources
import org.springframework.stereotype.Repository

@Repository
class UserResouceRepositoryNative : BaseRepositoryNative<UsersResources>(UsersResources::class.java)
