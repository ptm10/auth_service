package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.TaskNotification
import org.springframework.data.jpa.repository.JpaRepository

interface TaskNotificationRepository : JpaRepository<TaskNotification, String> {

    fun findByTaskIdAndTaskTypeAndActive(
        taskId: String?,
        taskType: Int?,
        active: Boolean? = true
    ): List<TaskNotification>

}
