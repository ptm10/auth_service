package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.Component
import id.go.kemenag.madrasah.pmrms.auth.pojo.Role
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface ComponentRepository : JpaRepository<Component, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<Component>
}
