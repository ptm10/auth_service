package id.go.kemenag.madrasah.pmrms.auth.service

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.auth.constant.*
import id.go.kemenag.madrasah.pmrms.auth.helpers.*
import id.go.kemenag.madrasah.pmrms.auth.model.request.PaginationRequest
import id.go.kemenag.madrasah.pmrms.auth.model.request.approval.ApproveRequest
import id.go.kemenag.madrasah.pmrms.auth.model.request.auth.*
import id.go.kemenag.madrasah.pmrms.auth.model.request.notif.EmailRequest
import id.go.kemenag.madrasah.pmrms.auth.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.model.response.jwt.JwtAuthenticationResponse
import id.go.kemenag.madrasah.pmrms.auth.pojo.*
import id.go.kemenag.madrasah.pmrms.auth.repository.*
import org.apache.commons.lang3.time.DateUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest


@Suppress("UNCHECKED_CAST", "NAME_SHADOWING")
@Service
class AuthService {

    @Autowired
    private lateinit var repoUser: UserRepository

    @Autowired
    private lateinit var repoVuserResources: VUserResourcesRepository

    @Autowired
    private lateinit var repoUserForgotPassword: UserForgotPasswordRepository

    @Autowired
    private lateinit var repoRole: RoleRepository

    @Autowired
    private lateinit var repoUpdateRoleRequest: UpdateRoleRequestRepository

    @Autowired
    private lateinit var repoUpdateRoleRequestDetail: UpdateRoleRequestDetailRepository

    @Autowired
    private lateinit var repoUsersRequiredChangePassword: UserRequiredChangePasswordRepository

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var serviceApproval: ApprovalService

    @Autowired
    private lateinit var repoTask: TaskRepository

    @Autowired
    private lateinit var repoTaskNotification: TaskNotificationRepository

    @Autowired
    private lateinit var repoUserRole: UserRoleRepository

    @Autowired
    private lateinit var repoChangeEmailRequest: ChangeEmailRequestRepository

    @Autowired
    private lateinit var repoLspDate: LspDateRepository

    @Value("\${service.notif.url}")
    private lateinit var notifUrl: String

    @Value("\${reset.password.url}")
    private lateinit var resetPasswordUrl: String

    @Value("\${change.email.url}")
    private lateinit var changeEmailUrl: String

    @Value("\${files.url}")
    private lateinit var filesUrl: String

    @Value("\${app.name}")
    private lateinit var appName: String

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var logActivityService: LogActivityService

    fun login(request: LoginRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validateLogin(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(message = "Pengguna $VALIDATOR_MSG_UNKNOWN")
            }

            val data = repoUser.findByEmailAndActive(request.email)
            if (data.isPresent) {
                if (BCryptPasswordEncoder().matches(request.password, data.get().password)) {
                    val findLsp = data.get().roles?.find { f -> // check expired date lsp
                        f.roleId == ROLE_ID_LSP
                    }

                    if (findLsp != null) {
                        val findLspDate = repoLspDate.findByUserIdAndActive(data.get().id)
                        if (findLspDate.isPresent) {
                            if (Date().before(findLspDate.get().startDate)) {
                                return responseBadRequest(message = "Pengguna $VALIDATOR_MSG_UNKNOWN")
                            }
                            if (Date().after(findLspDate.get().endDate)) {
                                return responseBadRequest(message = "Pengguna $VALIDATOR_MSG_UNKNOWN")
                            }
                        }
                    }

                    data.get().lastLogin = Date()
                    repoUser.save(data.get())

                    val objectMapper = ObjectMapper()
                    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                    val findVuserResources = repoVuserResources.findByIdAndActive(data.get().id)
                    return if (findVuserResources.isPresent) {
                        responseSuccess(data = writeToken(findVuserResources.get(), "user-admin"))
                    } else {
                        responseBadRequest(message = "Pengguna $VALIDATOR_MSG_UNKNOWN")
                    }
                } else {
                    return responseBadRequest(message = "Pengguna $VALIDATOR_MSG_UNKNOWN")
                }
            } else {
                return responseBadRequest(message = "Pengguna $VALIDATOR_MSG_UNKNOWN")
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun detail(): ResponseEntity<ReturnData> {
        try {
            getUserLogin()?.let {
                val data = repoVuserResources.findByIdAndActive(it.id)
                if (data.isPresent) {
                    return responseSuccess(data = data.get())
                }
            }
            return responseNotFound("User $VALIDATOR_MSG_NOT_FOUND")
        } catch (e: Exception) {
            throw e
        }
    }

    fun forgotPassword(request: ForgotPasswordRequest, test: Boolean = false): ResponseEntity<ReturnData> {
        try {
            if (!request.email!!.contains(EMAIL_VALID_REGEX)) {
                return responseNotFound("Pengguna $VALIDATOR_MSG_NOT_FOUND")
            }
            val data = repoUser.findByEmailAndActive(request.email)
            if (data.isPresent) {
                val userForgotPassword = UsersForgotPassword()
                userForgotPassword.userId = data.get().id
                userForgotPassword.expiredDate =
                    DateUtils.addHours(userForgotPassword.createdAt, RESET_PASSWORD_EXPIRED_HOUR)
                repoUserForgotPassword.save(userForgotPassword)

                val expiredDate = userForgotPassword.expiredDate
                val sdf = SimpleDateFormat("HH:mm")
                val expiredClock = sdf.format(expiredDate)

                if (!test) {
                    var message = "Anda sedang menyetel ulang password PMRMS Kementerian Agama Republik Indonesia<br/>"
                    message += "Silahkan klik link dibawah ini sebelum jam $expiredClock untuk mengkonfirmasi setel ulang password :<br/><br/>"
                    message += "<a href='${resetPasswordUrl + userForgotPassword.id}'>${resetPasswordUrl + userForgotPassword.id}</a>"

                    RequestHelpers.notifSendEmail(
                        notifUrl, EmailRequest(
                            data.get().email,
                            "Setel Ulang Password",
                            message
                        )
                    )
                }

                return responseSuccess(
                    data = mapOf(
                        "requestId" to userForgotPassword.id,
                        "expired" to expiredClock
                    )
                )
            }
            return responseNotFound("Pengguna $VALIDATOR_MSG_NOT_FOUND")
        } catch (e: Exception) {
            throw e
        }
    }

    fun resetPassword(request: ResetPasswordRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validateResetPassword(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val users: Users = validate["users"] as Users
            val usersForgotPassword: UsersForgotPassword = validate["usersForgotPassword"] as UsersForgotPassword

            users.password = BCryptPasswordEncoder().encode(request.password)
            repoUser.save(users)

            usersForgotPassword.active = false
            repoUserForgotPassword.save(usersForgotPassword)
            return responseSuccess(message = "Setel Ulang Password $MSG_SUCCESS")
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateProfile(request: UpdateProfileRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validateUpdateData(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }
            val findUser = repoUser.findByIdAndActive(getUserLogin()?.id)
            if (!findUser.isPresent) {
                return responseNotFound()
            }

            val update = findUser.get()

            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

            val oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), Users::class.java
            ) as Users

            if (!request.email.isNullOrEmpty()) {
                if (request.email != update.email) {
                    val chageEmail: ResponseEntity<ReturnData> =
                        changeEmail(ChangeEmailRequestRequestRequest(request.email))

                    chageEmail.body?.let {
                        it.success?.let { s ->
                            if (!s) {
                                return if (it.data != null) {
                                    responseBadRequest(data = it.data)
                                } else {
                                    responseBadRequest(data = it.message)
                                }
                            }
                        }
                    }
                }
            }

            update.firstName = request.firstName
            update.lastName = request.lastName

            if (!request.profilePictureId.isNullOrEmpty()) {
                if (request.profilePictureId != update.profilePictureId) {
                    RequestHelpers.filesDelete(
                        update.profilePictureId ?: "",
                        filesUrl,
                        httpServletRequest.getHeader(HEADER_STRING)
                    )
                }

                val files: Files = validate["files"] as Files
                update.profilePictureId = request.profilePictureId
                update.profilePicture = files
            }

            if (!request.roleRequest.isNullOrEmpty()) {
                val roles: Map<String, Role> = validate["roles"] as Map<String, Role>

                val updateRole = repoUpdateRoleRequest.save(
                    UpdateRoleRequest(userId = getUserLogin()?.id)
                )

                var taskDescription = ""
                var taskNotifMessage = ""
                val reqDetailSize = request.roleRequest?.size ?: 0

                request.roleRequest?.forEachIndexed { index, it ->
                    val updateRoleRequestDetail = repoUpdateRoleRequestDetail.save(
                        (UpdateRoleRequestDetail(
                            updateRoleRequestId = updateRole.id,
                            roleId = it.roleId,
                            role = roles[it.roleId],
                            requestStatus = it.status
                        ))
                    )
                    updateRole.detail?.add(updateRoleRequestDetail)

                    updateRoleRequestDetail.requestStatus?.let { d ->
                        var status = "Menghapus"
                        if (d == REQUEST_ROLE_STATUS_CREATE) {
                            status = "Menambahkan"
                        }

                        if (taskDescription != "") {
                            taskDescription += ", "
                        }

                        if (taskNotifMessage != "") {
                            taskNotifMessage += if (index != (reqDetailSize - 1)) {
                                ", "
                            } else {
                                " dan "
                            }
                        }

                        taskDescription += "$status Role ${updateRoleRequestDetail.role?.name}"
                        taskNotifMessage += "${status.lowercase()} role ${updateRoleRequestDetail.role?.name}"
                    }
                }

                repoTask.save(
                    Task(
                        createdBy = updateRole.userId,
                        taskType = TASK_TYPE_CHANGE_ROLE_REQUEST,
                        taskId = updateRole.id,
                        description = taskDescription
                    )
                )

                val userRoles = repoUserRole.findByRoleIdInAndActive(ROLES_CHANGE_ROLE_NOTIF)
                userRoles.forEach {
                    repoTaskNotification.save(
                        TaskNotification(
                            userId = it.userId,
                            taskType = TASK_TYPE_CHANGE_ROLE_REQUEST,
                            taskId = updateRole.id,
                            message = taskNotifMessage,
                            createdBy = getUserLogin()?.id
                        )
                    )
                }

                if (userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) { // user has role adminstrator
                    serviceApproval.approve(
                        ApproveRequest(
                            updateRole.id,
                            APPROVAL_TYPE_UPDATE_ROLE_REQUEST,
                            APPROVAL_STATUS_APPROVE
                        )
                    )
                }
            }

            repoUser.save(update)

            val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
            val userId = getUserLogin()?.id ?: ""
            Thread {
                logActivityService.writeChangeData(
                    update,
                    oldData,
                    "users",
                    ipAdress,
                    userId
                )
            }.start()

            return responseSuccess(data = update)
        } catch (e: Exception) {
            throw e
        }
    }

    fun updatePassword(request: UpdatePasswordRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validateUpdatePassword(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val users: Users = validate["users"] as Users
            users.password = BCryptPasswordEncoder().encode(request.confirmPassword)
            repoUser.save(users)

            val findRequiredChangePassword = repoUsersRequiredChangePassword.findByUserIdAndChanged(users.id)
            if (findRequiredChangePassword.isPresent) {
                findRequiredChangePassword.get().changed = true
                findRequiredChangePassword.get().updatedAt = Date()
                repoUsersRequiredChangePassword.save(findRequiredChangePassword.get())
            }

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    fun changeEmail(request: ChangeEmailRequestRequestRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validateChangeEmail(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val user = repoUser.findByIdAndActive(getUserLogin()?.id)
            if (user.isPresent) {
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val oldData = objectMapper.readValue(
                    objectMapper.writeValueAsString(user), Users::class.java
                ) as Users

                val data = repoChangeEmailRequest.save(
                    ChangeEmailRequest(
                        userId = getUserLogin()?.id,
                        email = request.email
                    )
                )

                val url = "$changeEmailUrl?id=${data.id}&identity=${
                    BCryptPasswordEncoder().encode(CONFIRM_EMAIL_FROM_OLD_EMAIL)
                }"
                val message =
                    "Untuk menyetujui perubahan email ke email ${data.email} silahkan klik link di bawah ini<br/> <a href='$url' target='_blank'>$url</a>"
                RequestHelpers.notifSendEmail(
                    notifUrl,
                    EmailRequest(getUserLogin()?.email, "Mengubah Email Aplikasi $appName", message)
                )

                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "change_email_request",
                        ipAdress,
                        userId
                    )
                }.start()

                return responseSuccess(
                    data = data
                )
            } else {
                return responseBadRequest(message = "Pengguna $VALIDATOR_MSG_UNKNOWN")
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun confirmChangeEmail(request: ConfirmChangeEmailRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validateConfirmChangeEmail(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }
            val changeRequest: ChangeEmailRequest = validate["request"] as ChangeEmailRequest

            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

            val oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(changeRequest), Users::class.java
            ) as Users

            if (request.status == APPROVAL_STATUS_REJECT) {
                changeRequest.active = false
            } else {
                val confirmFromOldEmail: Boolean = validate["confirmFromOldEmail"] as Boolean
                if (!confirmFromOldEmail) {
                    val url = "$changeEmailUrl?id=${changeRequest.id}&identity=${
                        BCryptPasswordEncoder().encode(CONFIRM_EMAIL_FROM_NEW_EMAIL)
                    }"
                    val message =
                        "Untuk mengganti email lama anda ${changeRequest.user?.email} ke email ini silahkan klik link di bawah ini untuk melakukan konfirmasi<br/> <a href='$url' target='_blank'>$url</a>"
                    RequestHelpers.notifSendEmail(
                        notifUrl,
                        EmailRequest(changeRequest.email, "Mengubah Email Aplikasi $appName", message)
                    )

                    changeRequest.confirmFromOldEmail = true
                } else {
                    changeRequest.user?.let {
                        it.email = changeRequest.email
                        it.updatedAt = Date()
                        repoUser.save(it)
                    }

                    changeRequest.confirmFromNewEmail = true
                    changeRequest.active = false
                }
            }

            changeRequest.updatedAt = Date()

            val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
            val userId = getUserLogin()?.id ?: ""
            Thread {
                logActivityService.writeChangeData(
                    changeRequest,
                    oldData,
                    "change_email_request",
                    ipAdress,
                    userId
                )
            }.start()

            repoChangeEmailRequest.save(changeRequest)

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    fun roles(page: PaginationRequest): ResponseEntity<ReturnData> {
        return try {
            val page: Page<Role?>? = if ((page.enablePage ?: 1) == 1) {
                var direction: Sort.Direction = Sort.Direction.ASC
                if (page.sort?.lowercase(Locale.getDefault()).equals("desc")) {
                    direction = Sort.Direction.DESC
                }
                repoRole.getListPagination(
                    param = page.param.toString().lowercase(Locale.getDefault()),
                    pageable = PageRequest.of(page.page ?: 0, page.size ?: 10, Sort.by(direction, page.sortBy))
                )
            } else {
                repoRole.getListPagination(
                    param = page.param.toString().lowercase(Locale.getDefault()),
                    pageable = Pageable.unpaged()
                )
            }
            responseSuccess(data = page)
        } catch (e: Exception) {
            throw e
        }
    }

    fun requiredChangePassword(): ResponseEntity<ReturnData> {
        try {
            var required = false
            val check = repoUsersRequiredChangePassword.findByUserIdAndChanged(getUserLogin()?.id)
            if (check.isPresent) {
                required = true
            }
            return responseSuccess(data = required)
        } catch (e: Exception) {
            throw e
        }
    }

    fun cancelUpdateRoleRequest(): ResponseEntity<ReturnData> {
        try {
            val find = repoUpdateRoleRequest.findByUserIdAndApproveStatusAndActive(
                getUserLogin()?.id ?: "",
                APPROVAL_STATUS_REQUEST
            )
            if (find.isPresent) {
                find.get().active = false
                find.get().updatedAt = Date()
                repoUpdateRoleRequest.save(find.get())

                val task = repoTask.findByTaskIdAndTaskTypeAndActive(find.get().id, TASK_TYPE_CHANGE_ROLE_REQUEST)
                if (task.isPresent) {
                    task.get().active = false
                    task.get().updatedAt = Date()
                    repoTask.save(task.get())
                }

                repoTaskNotification.findByTaskIdAndTaskTypeAndActive(find.get().id, TASK_TYPE_CHANGE_ROLE_REQUEST)
                    .forEach {
                        it.active = false
                        it.updatedAt = Date()
                        repoTaskNotification.save(it)
                    }

                return responseSuccess()
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validateChangeEmail(request: ChangeEmailRequestRequestRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (!request.email!!.contains(EMAIL_VALID_REGEX)) {
                listMessage.add(ErrorMessage("email", "Email $VALIDATOR_MSG_NOT_VALID"))
            }

            val user = repoUser.findByEmailAndActive(request.email)
            if (user.isPresent) {
                if (user.get().id != getUserLogin()?.id) {
                    listMessage.add(ErrorMessage("email", "Email ${request.email} $VALIDATOR_MSG_HAS_USED"))
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateConfirmChangeEmail(request: ConfirmChangeEmailRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()
            val data = repoChangeEmailRequest.findByIdAndActive(request.requestId)
            if (!data.isPresent) {
                listMessage.add(ErrorMessage("requestId", "Permintaan perubahan email $VALIDATOR_MSG_NOT_FOUND"))
            } else {
                if (data.get().user != null) {
                    rData["request"] = data.get()
                }

                if (request.status == APPROVAL_STATUS_APPROVE) {
                    val chekEmail = repoUser.findByEmailAndActive(data.get().email)
                    if (chekEmail.isPresent) {
                        if (chekEmail.get().id != data.get().id) {
                            listMessage.add(
                                ErrorMessage(
                                    "requestId",
                                    "Tidak dapat merubah email. Email ${data.get().email} $VALIDATOR_MSG_HAS_USED"
                                )
                            )
                        }
                    }
                }
            }

            if (!BCryptPasswordEncoder().matches(
                    CONFIRM_EMAIL_FROM_OLD_EMAIL,
                    request.confirmationIdentity,
                ) && !BCryptPasswordEncoder().matches(CONFIRM_EMAIL_FROM_NEW_EMAIL, request.confirmationIdentity)
            ) {
                listMessage.add(ErrorMessage("confirmationIdentity", "Identitas Konfirmasi $VALIDATOR_MSG_NOT_VALID"))
            }

            var confirmFromOldEmail = false
            if (!BCryptPasswordEncoder().matches(CONFIRM_EMAIL_FROM_OLD_EMAIL, request.confirmationIdentity)) {
                confirmFromOldEmail = true
            }

            rData["confirmFromOldEmail"] = confirmFromOldEmail

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    fun changeEmailRequest(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoChangeEmailRequest.getByUserIdAndActive(getUserLogin()?.id))
        } catch (e: Exception) {
            throw e
        }
    }

    fun getChangeEmailByIdentity(id: String?, identityId: String?): ResponseEntity<ReturnData> {
        try {
            var identity = 0

            if (BCryptPasswordEncoder().matches(CONFIRM_EMAIL_FROM_OLD_EMAIL, identityId)) {
                identity = 1
            }

            if (BCryptPasswordEncoder().matches(CONFIRM_EMAIL_FROM_NEW_EMAIL, identityId)) {
                identity = 2
            }

            var oldEmail: String? = null
            var newEmail: String? = null
            var confirmFromOldEmail: Boolean? = null
            var confirmFromNewEmail: Boolean? = null

            val request = repoChangeEmailRequest.findByIdAndActive(id)
            if (request.isPresent) {
                oldEmail = request.get().user?.email
                newEmail = request.get().email
                confirmFromOldEmail = request.get().confirmFromOldEmail
                confirmFromNewEmail = request.get().confirmFromNewEmail
            }

            return responseSuccess(
                data = mapOf(
                    "identity" to identity,
                    "oldEmail" to oldEmail,
                    "newEmail" to newEmail,
                    "confirmFromOldEmail" to confirmFromOldEmail,
                    "confirmFromNewEmail" to confirmFromNewEmail
                )
            )
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validateUpdatePassword(request: UpdatePasswordRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()
            val findUser = repoUser.findByIdAndActive(getUserLogin()?.id)

            if (!findUser.isPresent) {
                listMessage.add(ErrorMessage("oldPassword", "User $VALIDATOR_MSG_NOT_FOUND"))
            } else {
                rData["users"] = findUser.get()
            }

            if (!BCryptPasswordEncoder().matches(request.oldPassword, findUser.get().password)) {
                listMessage.add(ErrorMessage("oldPassword", "Password Lama Salah"))
            }

            if (!request.newPassword.isNullOrEmpty()) {
                if (!request.newPassword!!.matches(Regex(PASSWORD_REGEX))) {
                    listMessage.add(
                        ErrorMessage(
                            "newPassword",
                            "Password Baru minimal 8 karakter, 1 huruf besar, 1 huruf kecil, mempunyai angka dan simbol"
                        )
                    )
                }
            }

            if (!request.newPassword.isNullOrEmpty() && !request.confirmPassword.isNullOrEmpty()) {
                if (request.newPassword != request.confirmPassword) {
                    listMessage.add(
                        ErrorMessage(
                            "confirmPassword",
                            "Konfirmasi Password salah"
                        )
                    )
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateUpdateData(request: UpdateProfileRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (!request.profilePictureId.isNullOrEmpty()) {
                val chekFile: ReturnData = RequestHelpers.filesGetById(
                    request.profilePictureId ?: "",
                    filesUrl,
                    httpServletRequest.getHeader(HEADER_STRING)
                )
                if (!chekFile.success!!) {
                    listMessage.add(ErrorMessage("profilePictureId", chekFile.message))
                } else {
                    rData["files"] = chekFile.data as Files
                }
            }

            val roles = mutableMapOf<String, Role>()

            if (!request.roleRequest.isNullOrEmpty()) {
                val find = repoUpdateRoleRequest.findByUserIdAndApproveStatusAndActive(
                    getUserLogin()?.id ?: "",
                    APPROVAL_STATUS_REQUEST
                )
                if (find.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "roleRequest",
                            "User masih memiliki permintaan perubahan role yang belum di proses"
                        )
                    )
                } else {
                    val allowedStatus = listOf(REQUEST_ROLE_STATUS_REMOVE, REQUEST_ROLE_STATUS_CREATE)

                    request.roleRequest?.forEach {
                        val findRole = repoRole.findByIdAndActive(it.roleId)
                        if (!findRole.isPresent) {
                            listMessage.add(
                                ErrorMessage(
                                    "roleRequest",
                                    "Role Id ${it.roleId} $VALIDATOR_MSG_NOT_FOUND"
                                )
                            )
                        } else {
                            if (!allowedStatus.contains(it.status)) {
                                ErrorMessage(
                                    "roleRequest",
                                    "Status Role ${it.roleId} $VALIDATOR_MSG_NOT_VALID"
                                )
                            } else {
                                val checkUserRole =
                                    repoUserRole.findByUserIdAndRoleIdAndActive(getUserLogin()?.id, it.roleId)

                                if (it.status == REQUEST_ROLE_STATUS_CREATE) {
                                    if (checkUserRole.isPresent) {
                                        listMessage.add(
                                            ErrorMessage(
                                                "roleRequest",
                                                "User sudah mempunyai role ${findRole.get().name}"
                                            )
                                        )
                                    }
                                } else {
                                    if (!checkUserRole.isPresent) {
                                        listMessage.add(
                                            ErrorMessage(
                                                "roleRequest",
                                                "User tidak mempunyai role ${findRole.get().name}"
                                            )
                                        )
                                    }
                                }
                            }

                            roles[findRole.get().id ?: ""] = findRole.get()
                        }
                    }
                }

                val roleUser: MutableSet<UsersRole> = getUserRoles()
                val newRole: MutableList<Role> = mutableListOf()
                val allRole = repoRole.getAllByActive()

                roleUser.forEach { ru ->
                    ru.role?.let { newRole.add(it) }
                }

                request.roleRequest?.forEach { rr ->
                    if (rr.status == REQUEST_ROLE_STATUS_REMOVE) {
                        newRole.remove(newRole.find { nrf -> nrf.id == rr.roleId })
                    } else {
                        allRole.find { nrf -> nrf.id == rr.roleId }?.let { newRole.add(it) }
                    }
                }

                listMessage.addAll(userService.validateRole(newRole))
            }

            if (listMessage.isEmpty()) {
                if (!request.roleRequest.isNullOrEmpty()) {
                    val userRole: MutableList<UsersRole> = getUserRoles().toMutableList()

                    request.roleRequest?.forEach {
                        if (it.status == REQUEST_ROLE_STATUS_REMOVE) {
                            userRole.removeIf { r ->
                                r.id == it.roleId
                            }
                        }
                    }
                }
            }

            rData["roles"] = roles
            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateLogin(request: LoginRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (!request.email!!.contains(EMAIL_VALID_REGEX)) {
                listMessage.add(ErrorMessage("email", ""))
            }

            if (!request.password!!.matches(Regex(PASSWORD_REGEX))) {
                listMessage.add(ErrorMessage("password", ""))
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateResetPassword(request: ResetPasswordRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (!request.password!!.matches(Regex(PASSWORD_REGEX))) {
                listMessage.add(
                    ErrorMessage(
                        "password",
                        "Minimal 8 karakter, 1 huruf besar, 1 huruf kecil, mempunyai angka dan simbol"
                    )
                )
            }

            if (!request.password.equals(request.confirmPassword)) {
                listMessage.add(ErrorMessage("confirmPassword", "Konfirmasi Password salah"))
            }

            if (listMessage.isEmpty()) {
                val userForgotPassword = repoUserForgotPassword.findByIdAndActive(request.requestId)
                val userForgotPasswordExpMsg = "Permintaan setel ulang password sudah expired"

                if (userForgotPassword.isPresent) {
                    rData["usersForgotPassword"] = userForgotPassword.get()
                    val dform: DateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")

                    if (Date().after(dform.parse(dform.format(userForgotPassword.get().expiredDate)))) {
                        listMessage.add(ErrorMessage("requestId", userForgotPasswordExpMsg))
                    } else {
                        val user = repoUser.findByIdAndActive(userForgotPassword.get().userId)
                        if (user.isPresent) {
                            rData["users"] = user.get()
                        } else {
                            listMessage.add(ErrorMessage("requestId", "User $VALIDATOR_MSG_NOT_FOUND"))
                        }
                    }
                } else {
                    listMessage.add(ErrorMessage("requestId", userForgotPasswordExpMsg))
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun writeToken(user: VUsersResources, audience: String): JwtAuthenticationResponse {
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        val users: VUsersResources =
            objectMapper.readValue(objectMapper.writeValueAsString(user), VUsersResources::class.java)

        val expire = Date(System.currentTimeMillis() + EXPIRATION_TIME)
        val token =
            JWT.create().withSubject(objectMapper.writeValueAsString(user)).withExpiresAt(expire).withAudience(audience)
                .sign(Algorithm.HMAC512(SECRET.toByteArray()))

        var requiredChagePassword = false

        val findRequired = repoUsersRequiredChangePassword.findByUserIdAndChanged(users.id)
        if (findRequired.isPresent) {
            requiredChagePassword = true
        }

        return JwtAuthenticationResponse(
            accessToken = token,
            tokenType = TOKEN_PREFIX.trim(),
            expired = expire.time, users,
            requiredChagePassword = requiredChagePassword
        )
    }

    fun forgotPasswordValidate(id: String): ResponseEntity<ReturnData> {
        var valid = false
        try {
            val findData = repoUserForgotPassword.findByIdAndActive(id)
            if (findData.isPresent) {
                val dform: DateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
                if (Date().before(dform.parse(dform.format(findData.get().expiredDate)))) {
                    valid = true
                } else {
                    findData.get().active = false
                    repoUserForgotPassword.save(findData.get())
                }
            }
            return responseSuccess(data = valid)
        } catch (e: Exception) {
            throw e
        }
    }

    fun cancelUpdateEmail(): ResponseEntity<ReturnData> {
        try {
            repoChangeEmailRequest.getByUserIdAndActive(getUserLogin()?.id).forEach {
                it.active = false
                it.updatedAt = Date()
                repoChangeEmailRequest.save(it)
            }
            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }
}
