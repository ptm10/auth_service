package id.go.kemenag.madrasah.pmrms.auth.service

import id.go.kemenag.madrasah.pmrms.auth.constant.*
import id.go.kemenag.madrasah.pmrms.auth.helpers.*
import id.go.kemenag.madrasah.pmrms.auth.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.auth.model.request.ParamArray
import id.go.kemenag.madrasah.pmrms.auth.model.request.ParamSearch
import id.go.kemenag.madrasah.pmrms.auth.model.request.approval.ApproveRequest
import id.go.kemenag.madrasah.pmrms.auth.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.pojo.UsersRole
import id.go.kemenag.madrasah.pmrms.auth.repository.*
import id.go.kemenag.madrasah.pmrms.auth.repository.native.UpdateRoleRequestRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*

@Service
class ApprovalService {

    @Autowired
    private lateinit var repoUpdateRoleRequest: UpdateRoleRequestRepository

    @Autowired
    private lateinit var repoNativeUpdateRoleRequest: UpdateRoleRequestRepositoryNative

    @Autowired
    private lateinit var repoUsers: UserRepository

    @Autowired
    private lateinit var repoUserRole: UserRoleRepository

    @Autowired
    private lateinit var repoTask: TaskRepository

    @Autowired
    private lateinit var repoTaskNotification: TaskNotificationRepository

    fun approve(request: ApproveRequest): ResponseEntity<ReturnData> {
        try {
            if (request.type == APPROVAL_TYPE_UPDATE_ROLE_REQUEST) {
                val checkRole: List<UsersRole>? = getUserLogin()?.roles?.filter {
                    it.roleId == ROLE_ID_ADMINSTRATOR
                }
                if (checkRole.isNullOrEmpty()) {
                    return responseUnautorized()
                }

                val findData = repoUpdateRoleRequest.findByIdAndActive(request.approvalId)
                if (!findData.isPresent) {
                    return responseBadRequest(message = "Update Role Request ${request.approvalId} $VALIDATOR_MSG_NOT_FOUND")
                }

                val data = findData.get()

                if (request.approveStatus == APPROVAL_STATUS_APPROVE) {
                    val findUser = repoUsers.findByIdAndActive(data.userId)
                    if (!findUser.isPresent) {
                        return responseBadRequest(message = "Users Update Role Request ${request.approvalId} $VALIDATOR_MSG_NOT_FOUND")
                    }

                    data.detail?.forEach {
                        val findUsersRole = repoUserRole.findByUserIdAndRoleIdAndActive(data.userId, it.roleId)
                        if (it.requestStatus == REQUEST_ROLE_STATUS_REMOVE) {
                            if (findUsersRole.isPresent) {
                                findUsersRole.get().active = false
                                repoUserRole.save(findUsersRole.get())
                            }
                        } else {
                            if (!findUsersRole.isPresent) {
                                repoUserRole.save(
                                    UsersRole(
                                        userId = data.userId,
                                        roleId = it.roleId
                                    )
                                )
                            }
                        }
                    }
                } else {
                    if (request.rejectedMessage.isNullOrEmpty()) {
                        return responseBadRequest(
                            data = listOf(
                                ErrorMessage(
                                    "rejectedMessage",
                                    "Alasan Penolakan $VALIDATOR_MSG_REQUIRED"
                                )
                            )
                        )
                    } else {
                        data.rejectedMessage = request.rejectedMessage
                    }
                }

                data.approveStatus = request.approveStatus
                data.updatedBy = getUserLogin()?.id
                data.updatedAt = Date()

                repoUpdateRoleRequest.save(data)

                if (request.approveStatus == APPROVAL_STATUS_APPROVE || request.approveStatus == APPROVAL_STATUS_REJECT) {
                    var taskStatus = TASK_STATUS_APPROVED
                    if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                        taskStatus = TASK_STATUS_REJECT
                    }

                    val task = repoTask.findByTaskIdAndTaskTypeAndActive(data.id, TASK_TYPE_CHANGE_ROLE_REQUEST)
                    if (task.isPresent) {
                        task.get().done = true
                        task.get().updatedBy = getUserLogin()?.id
                        task.get().status = taskStatus
                        repoTask.save(task.get())
                    }

                    repoTaskNotification.findByTaskIdAndTaskTypeAndActive(data.id, TASK_TYPE_CHANGE_ROLE_REQUEST)
                        .forEach {
                            it.active = false
                            repoTaskNotification.save(it)
                        }
                }


                return responseSuccess()
            }

            return responseBadRequest(message = "Approve Type ${request.type} $VALIDATOR_MSG_NOT_FOUND")
        } catch (e: Exception) {
            throw e
        }
    }

    fun pendingUpdateRoleRequest(): ResponseEntity<ReturnData> {
        try {
            val find = repoUpdateRoleRequest.findByUserIdAndApproveStatusAndActive(
                getUserLogin()?.id ?: "",
                APPROVAL_STATUS_REQUEST
            )
            if (find.isPresent) {
                return responseSuccess(data = find.get())
            }
            return responseSuccess(data = null)
        } catch (e: Exception) {
            throw e
        }
    }

    fun roleRequestList(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            req.paramNotIn?.add(
                ParamArray(
                    "userId",
                    "string",
                    listOf(getUserLogin()?.id ?: "")
                )
            )

            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                req.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
            }

            responseSuccess(data = repoNativeUpdateRoleRequest.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repoUpdateRoleRequest.findByIdAndActive(id)
            if (data.isPresent) {
                return responseSuccess(data = data)
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }
}
