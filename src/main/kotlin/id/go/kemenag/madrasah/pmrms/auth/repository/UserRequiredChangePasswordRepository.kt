package id.go.kemenag.madrasah.pmrms.auth.repository


import id.go.kemenag.madrasah.pmrms.auth.pojo.UsersRequiredChangePassword
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserRequiredChangePasswordRepository : JpaRepository<UsersRequiredChangePassword, String> {

    fun findByUserIdAndChanged(userId: String?, active: Boolean? = false): Optional<UsersRequiredChangePassword>
}
