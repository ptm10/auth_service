package id.go.kemenag.madrasah.pmrms.auth.model.response

data class ErrorMessage(
    var key: String? = null,
    var message: String? = null
)
