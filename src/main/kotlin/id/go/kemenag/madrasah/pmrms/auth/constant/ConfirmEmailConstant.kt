package id.go.kemenag.madrasah.pmrms.auth.constant

const val CONFIRM_EMAIL_FROM_OLD_EMAIL = "OLD"

const val CONFIRM_EMAIL_FROM_NEW_EMAIL = "NEW"
