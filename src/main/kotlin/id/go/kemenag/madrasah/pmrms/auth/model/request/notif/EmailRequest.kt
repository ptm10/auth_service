package id.go.kemenag.madrasah.pmrms.auth.model.request.notif

data class EmailRequest(

    var to: String? = null,

    var subject: String? = null,

    var message: String? = null
)

