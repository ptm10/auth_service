package id.go.kemenag.madrasah.pmrms.auth.repository.native

import id.go.kemenag.madrasah.pmrms.auth.pojo.VUsersResources
import org.springframework.stereotype.Repository

@Repository
class VUserResouceRepositoryNative : BaseRepositoryNative<VUsersResources>(VUsersResources::class.java)
