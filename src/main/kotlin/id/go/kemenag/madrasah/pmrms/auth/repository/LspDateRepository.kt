package id.go.kemenag.madrasah.pmrms.auth.repository


import id.go.kemenag.madrasah.pmrms.auth.pojo.LspDate
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface LspDateRepository : JpaRepository<LspDate, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<LspDate>

    fun findByUserIdAndActive(userId: String?, active: Boolean = true): Optional<LspDate>
}
