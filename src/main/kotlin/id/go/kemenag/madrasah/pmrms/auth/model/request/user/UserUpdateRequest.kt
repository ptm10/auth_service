package id.go.kemenag.madrasah.pmrms.auth.model.request.user

import id.go.kemenag.madrasah.pmrms.auth.constant.VALIDATOR_MSG_REQUIRED
import id.go.kemenag.madrasah.pmrms.auth.validator.EmailValidator
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class UserUpdateRequest(

    @field:NotEmpty(message = "Nama Depan $VALIDATOR_MSG_REQUIRED")
    var firstName: String? = null,

    var lastName: String? = null,

    @field:NotEmpty(message = "Email $VALIDATOR_MSG_REQUIRED")
    @field:EmailValidator
    var email: String? = null,

    var componentId: String? = null,

    var provinceId: String? = null,

    var profilePictureId: String? = null,

    var roles: List<RoleRequest>? = null,

    @field:NotNull(message = "Status Active $VALIDATOR_MSG_REQUIRED")
    var active: Boolean? = null,

    var lspStartDate: String? = null,

    var lspEndDate: String? = null,

    var lspAwpImplementationIds: List<String>? = null,

    var removeLspAwpImplementationIds: List<String>? = null,
)

data class RoleRequest(

    var roleId: String? = null,

    var status: Int? = null
)

