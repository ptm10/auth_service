package id.go.kemenag.madrasah.pmrms.auth.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.auth.constant.*
import id.go.kemenag.madrasah.pmrms.auth.exception.UnautorizedException
import id.go.kemenag.madrasah.pmrms.auth.helpers.*
import id.go.kemenag.madrasah.pmrms.auth.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.auth.model.request.ParamArray
import id.go.kemenag.madrasah.pmrms.auth.model.request.ParamSearch
import id.go.kemenag.madrasah.pmrms.auth.model.request.Sort
import id.go.kemenag.madrasah.pmrms.auth.model.request.notif.EmailRequest
import id.go.kemenag.madrasah.pmrms.auth.model.request.user.UserSaveRequest
import id.go.kemenag.madrasah.pmrms.auth.model.request.user.UserUpdateRequest
import id.go.kemenag.madrasah.pmrms.auth.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.pojo.*
import id.go.kemenag.madrasah.pmrms.auth.repository.*
import id.go.kemenag.madrasah.pmrms.auth.repository.native.UserResouceRepositoryNative
import id.go.kemenag.madrasah.pmrms.auth.repository.native.VUserResouceRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest

@Suppress("UNCHECKED_CAST")
@Service
class UserService {

    @Autowired
    private lateinit var repo: UserRepository

    @Autowired
    private lateinit var repoRole: RoleRepository

    @Autowired
    private lateinit var repoUserRole: UserRoleRepository

    @Autowired
    private lateinit var repoUserResouceRepositoryNative: UserResouceRepositoryNative

    @Autowired
    private lateinit var userRequiredChangePassword: UserRequiredChangePasswordRepository

    @Autowired
    private lateinit var repoVUserResouceRepositoryNative: VUserResouceRepositoryNative

    @Autowired
    private lateinit var repoVUserResources: VUserResourcesRepository

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var repoLspDate: LspDateRepository

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoUserRoleUser: UserRoleUserRepository

    @Autowired
    private lateinit var repoAwpImplementation: AwpImplementationRepository

    @Autowired
    private lateinit var repoProvince: ProvinceRepository

    @Autowired
    private lateinit var logActivityService: LogActivityService

    @Value("\${files.url}")
    private lateinit var filesUrl: String

    @Value("\${service.notif.url}")
    private lateinit var notifUrl: String

    @Value("\${web.url}")
    private lateinit var webUrl: String

    private val supervisiorChild: MutableList<Role> = mutableListOf()

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        try {
            repoUserResouceRepositoryNative.setActiveData(false)
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val searchRole = req.paramIn?.filter { f ->
                f.field == "roleIds"
            }

            val roleIds = mutableListOf<String>()
            searchRole?.forEach {
                it.value?.forEach { t ->
                    roleIds.add(t)
                }
            }

            if (roleIds.isNotEmpty()) {
                searchRole?.let { req.paramIn?.removeAll(it) }
                req.paramIn?.add(
                    ParamArray("id",
                        "string",
                        repoUserRole.selectUserIdByRoleInAndActive(roleIds).filter { f -> f != getUserLogin()?.id })
                )
            }

            return responseSuccess(data = repoVUserResouceRepositoryNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatableLsp(req: Pagination2Request): ResponseEntity<ReturnData> {
        try {
            val findComponent: ParamSearch? = req.paramIs?.find { f ->
                f.field == "componentId"
            }

            val userIds: List<String>
            if (findComponent != null) {
                repoUserRoleUser.getUserIdsByRoleIdsComponentId(listOf(ROLE_ID_LSP), 1, findComponent.value)
                    .also { userIds = it }
                req.paramIs?.remove(findComponent)
            } else {
                repoUserRoleUser.getUserIdsByRoleIds(listOf(ROLE_ID_LSP), 1).also { userIds = it }
            }

            if (userIds.isNotEmpty()) {
                req.paramIn?.add(ParamArray("id", "string", userIds))
                req.sort?.add(Sort("firstName", "asc"))
                req.sort?.add(Sort("lastName", "asc"))
            } else {
                req.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
            }

            return responseSuccess(data = repoVUserResouceRepositoryNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun save(request: UserSaveRequest): ResponseEntity<ReturnData> {
        try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val validate = validate(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val password = generateRandomPassword()
            val create = Users(
                firstName = request.firstName,
                lastName = request.lastName,
                email = request.email,
                password = BCryptPasswordEncoder().encode(password)
            )

            if (!request.profilePictureId.isNullOrEmpty()) {
                val files: Files = validate["files"] as Files
                create.profilePictureId = request.profilePictureId
                create.profilePicture = files
            }

            if (!request.componentId.isNullOrEmpty()) {
                val component: Component = validate["component"] as Component
                create.componentId = request.componentId
                create.component = component
            } else {
                create.componentId = COMPONENT_OTHER_ID
            }

            if (!request.provinceId.isNullOrEmpty()) {
                val province: Province = validate["province"] as Province
                create.provinceId = request.provinceId
                create.province = province
            } else {
                create.provinceId = PROVINCE_OTHER_ID
            }

            repo.save(create)

            val findLspDate = repoLspDate.findByUserIdAndActive(create.id)

            if (request.roles?.contains(ROLE_ID_LSP) == true) {
                var lspDate = LspDate()
                lspDate.userId = create.id
                if (findLspDate.isPresent) {
                    lspDate = findLspDate.get()
                }
                lspDate.startDate = validate["lspStartDate"] as Date
                lspDate.endDate = validate["lspEndDate"] as Date

                repoLspDate.save(lspDate)

                val resources = repoResources.findByUserIdAndActive(create.id)
                if (resources.isPresent) {
                    resources.get().active = false
                    resources.get().updatedAt = Date()
                    repoResources.save(resources.get())
                }

                val awpImplementation: List<AwpImplementation> =
                    validate["awpImplementation"] as List<AwpImplementation>
                awpImplementation.forEach {
                    it.lspPic = create.id
                    repoAwpImplementation.save(it)
                }
            } else {
                if (findLspDate.isPresent) {
                    findLspDate.get().updatedAt = Date()
                    findLspDate.get().active = false
                    repoLspDate.save(findLspDate.get())
                }
            }

            val createdRoles: MutableSet<UsersRole> = mutableSetOf()
            val roles: MutableList<Role> = validate["roles"] as MutableList<Role>

            request.roles?.forEach {
                createdRoles.add(
                    repoUserRole.save(
                        UsersRole(userId = create.id, roleId = it, role = roles.find { f -> f.id == it })
                    )
                )
            }

            create.roles = createdRoles

            userRequiredChangePassword.save(
                UsersRequiredChangePassword(
                    userId = create.id
                )
            )

            var message = "Anda sudah terdaftar di website <a href='$webUrl' target='_blank'>$webUrl<a><br/>"
            message += "Username : ${create.email}<br/>"
            message += "Password : $password"

            RequestHelpers.notifSendEmail(
                notifUrl, EmailRequest(
                    create.email, "Registrasi User", message
                ), ""
            )

            return responseCreated(data = create)
        } catch (e: Exception) {
            throw e
        }
    }

    fun update(id: String, request: UserUpdateRequest): ResponseEntity<ReturnData>? {
        try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val find = repo.findById(id)
            if (find.isPresent) {
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val oldData = objectMapper.readValue(
                    objectMapper.writeValueAsString(find), Users::class.java
                ) as Users


                val validate = validateUpdate(find.get(), request)
                val listMessage = validate["listMessage"] as List<ErrorMessage>
                if (listMessage.isNotEmpty()) {
                    return responseBadRequest(data = listMessage)
                }

                val update = find.get()

                if (update.componentId != request.componentId) {
                    repoAwpImplementation.getByLspPicAndComponentIdAndActive(update.id, update.componentId).forEach {
                        it.lspPic = null
                        repoAwpImplementation.save(it)
                    }
                }

                update.email = request.email
                update.firstName = request.firstName
                update.lastName = request.lastName
                update.active = request.active


                if (!request.componentId.isNullOrEmpty()) {
                    val component: Component = validate["component"] as Component
                    update.componentId = request.componentId
                    update.component = component
                } else {
                    update.componentId = COMPONENT_OTHER_ID
                }

                if (!request.provinceId.isNullOrEmpty()) {
                    val province: Province = validate["province"] as Province
                    update.provinceId = request.provinceId
                    update.province = province
                } else {
                    update.provinceId = PROVINCE_OTHER_ID
                }

                if (!request.profilePictureId.isNullOrEmpty()) {
                    val files: Files = validate["files"] as Files

                    update.profilePictureId = request.profilePictureId
                    update.profilePicture = files
                }

                if (!request.roles.isNullOrEmpty()) {
                    val roles: List<Role> = validate["roles"] as List<Role>

                    request.roles?.forEach {
                        val findUserRole = repoUserRole.findByUserIdAndRoleIdAndActive(update.id, it.roleId)
                        if (it.status == REQUEST_ROLE_STATUS_REMOVE) {
                            if (findUserRole.isPresent) {
                                findUserRole.get().active = false
                                repoUserRole.save(findUserRole.get())
                                update.roles?.removeIf { rr ->
                                    rr.id == it.roleId
                                }
                            }
                        } else {
                            if (!findUserRole.isPresent) {
                                val cUserRole =
                                    UsersRole(userId = update.id, roleId = it.roleId, role = roles.find { fr ->
                                        fr.id == it.roleId
                                    })
                                repoUserRole.save(cUserRole)
                                update.roles?.add(cUserRole)
                            }
                        }
                    }
                }

                var password = ""
                if (find.get().email != request.email) {
                    password = generateRandomPassword()
                    update.email = request.email
                    update.password = BCryptPasswordEncoder().encode(password)
                }

                update.updatedAt = Date()
                repo.save(update)

                val filterLsp = update.roles?.filter {
                    it.roleId == ROLE_ID_LSP
                }

                val findLspDate = repoLspDate.findByUserIdAndActive(update.id)
                if (filterLsp?.isNotEmpty() == true) {
                    var lspDate = LspDate()
                    lspDate.userId = update.id
                    if (findLspDate.isPresent) {
                        lspDate = findLspDate.get()
                    }

                    try {
                        lspDate.startDate = validate["lspStartDate"] as Date
                        lspDate.endDate = validate["lspEndDate"] as Date
                    } catch (_: Exception) {

                    }

                    repoLspDate.save(lspDate)

                    val resources = repoResources.findByUserIdAndActive(update.id)
                    if (resources.isPresent) {
                        resources.get().active = false
                        resources.get().updatedAt = Date()
                        repoResources.save(resources.get())
                    }

                    request.removeLspAwpImplementationIds?.forEach {
                        val findRemove = repoAwpImplementation.findByIdAndActive(it)
                        if (findRemove.isPresent) {
                            findLspDate.get().updatedAt = Date()
                            findRemove.get().lspPic = null
                            repoAwpImplementation.save(findRemove.get())
                        }
                    }

                    val awpImplementation: List<AwpImplementation> =
                        validate["awpImplementation"] as List<AwpImplementation>
                    awpImplementation.forEach {
                        it.updatedAt = Date()
                        it.lspPic = update.id
                        repoAwpImplementation.save(it)
                    }
                } else {
                    if (findLspDate.isPresent) {
                        findLspDate.get().updatedAt = Date()
                        findLspDate.get().active = false
                        repoLspDate.save(findLspDate.get())
                    }
                }

                if (password != "") {
                    var message = "Anda sudah terdaftar di website <a href='$webUrl' target='_blank'>$webUrl<a><br/>"
                    message += "Username : ${update.email}<br/>"
                    message += "Password : $password"

                    RequestHelpers.notifSendEmail(
                        notifUrl, EmailRequest(
                            update.email, "Registrasi User", message
                        ), ""
                    )
                }

                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        update,
                        oldData,
                        "users",
                        ipAdress,
                        userId
                    )
                }.start()

                return responseSuccess(data = update)
            }

            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
            throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
        }
        try {
            val data = repoVUserResources.findById(id)
            if (data.isPresent) {
                return responseSuccess(data = data)
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(request: UserSaveRequest): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (!request.email!!.contains(EMAIL_VALID_REGEX)) {
                listMessage.add(ErrorMessage("email", "Email $VALIDATOR_MSG_NOT_VALID"))
            }
            val checkEmail = repo.findByEmailAndActive(request.email!!)
            if (checkEmail.isPresent) {
                listMessage.add(ErrorMessage("email", "Email $VALIDATOR_MSG_HAS_USED"))
            }

            val selectedRole = mutableListOf<Role>()

            request.roles?.forEach {
                val check = repoRole.findByIdAndActive(it)
                if (!check.isPresent) {
                    listMessage.add(ErrorMessage("roles", "Role id $it $VALIDATOR_MSG_NOT_FOUND"))
                } else {
                    selectedRole.add(check.get())
                }
            }

            listMessage.addAll(validateRole(selectedRole))

            if (!request.profilePictureId.isNullOrEmpty()) {
                val chekFile: ReturnData = RequestHelpers.filesGetById(
                    request.profilePictureId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )
                if (!chekFile.success!!) {
                    listMessage.add(ErrorMessage("profilePictureId", chekFile.message))
                } else {
                    rData["files"] = chekFile.data as Files
                }
            }

            if (!request.componentId.isNullOrEmpty()) {
                val checkComponent = repoComponent.findByIdAndActive(request.componentId)
                if (!checkComponent.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "componentId", "Id Komponent ${request.componentId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    rData["component"] = checkComponent.get()
                }
            }

            if (!request.provinceId.isNullOrEmpty()) {
                val checkProvince = repoProvince.findByIdAndActive(request.provinceId)
                if (!checkProvince.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "provinceId", "Id Province ${request.provinceId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    rData["province"] = checkProvince.get()
                }
            }

            if (request.roles?.contains(ROLE_ID_LSP) == true) {
                var lspStartDate: Date? = null
                var lspEndDate: Date? = null

                val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

                if (request.lspStartDate.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "lspStartDate", "Tanggal Mulai LSP $VALIDATOR_MSG_REQUIRED"
                        )
                    )
                } else {
                    try {
                        lspStartDate = dform.parse(request.lspStartDate)
                    } catch (_: Exception) {
                        listMessage.add(
                            ErrorMessage(
                                "lspExpiredDate", "Tanggal Mulai LSP $VALIDATOR_MSG_NOT_VALID"
                            )
                        )
                    }
                }

                if (request.lspEndDate.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "lspEndDate", "Tanggal Akhir LSP $VALIDATOR_MSG_REQUIRED"
                        )
                    )
                } else {
                    try {
                        lspEndDate = dform.parse(request.lspEndDate)
                    } catch (_: Exception) {
                        listMessage.add(
                            ErrorMessage(
                                "lspEndDate", "Tanggal Akhir LSP $VALIDATOR_MSG_NOT_VALID"
                            )
                        )
                    }
                }

                if (lspStartDate != null) {
                    rData["lspStartDate"] = lspStartDate
                }

                if (lspEndDate != null) {
                    if (lspEndDate.before(lspStartDate)) {
                        listMessage.add(
                            ErrorMessage(
                                "lspEndDate", "Tanggal Akhir LSP $VALIDATOR_MSG_NOT_VALID"
                            )
                        )
                    }

                    rData["lspEndDate"] = lspEndDate
                }

                val awpImplementations = mutableListOf<AwpImplementation>()
                request.lspAwpImplementationIds?.forEach {
                    val checkAwp = repoAwpImplementation.findByIdAndActive(it)
                    if (!checkAwp.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "lspAwpImplementationId",
                                "Awp Implementation id ${request.lspAwpImplementationIds} $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    } else {
                        awpImplementations.add(checkAwp.get())
                    }
                }
                rData["awpImplementation"] = awpImplementations
            }


            if (request.roles?.contains(ROLE_ID_PCU) == true) {
                val userIds = repoUserRoleUser.getUserIdsByRoleIds(listOf(ROLE_ID_PCU), 1)
                if (userIds.isNotEmpty()) {
                    val usersPcu = repo.getByIdInAndProvinceIdAndActive(userIds, request.provinceId)
                    if (usersPcu.isNotEmpty()) {
                        listMessage.add(
                            ErrorMessage(
                                "roles", "User PCU Provinsi ${usersPcu[0].province?.name} $VALIDATOR_MSG_HAS_ADDED"
                            )
                        )
                    }
                }
            }

            rData["roles"] = selectedRole
            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateUpdate(user: Users, request: UserUpdateRequest): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (!request.email!!.contains(EMAIL_VALID_REGEX)) {
                listMessage.add(ErrorMessage("email", "Email $VALIDATOR_MSG_NOT_VALID"))
            }

            val findEmail = repo.findByEmailAndActive(request.email)
            if (findEmail.isPresent) {
                if (findEmail.get().id != user.id) {
                    listMessage.add(ErrorMessage("email", "Email ${request.email} $VALIDATOR_MSG_HAS_USED"))
                }
            }

            request.roles?.forEach {
                val check = repoRole.findByIdAndActive(it.roleId)
                if (!check.isPresent) {
                    listMessage.add(ErrorMessage("roles", "Role id ${it.roleId} $VALIDATOR_MSG_NOT_FOUND"))
                } else {
                    val findUserRole: UsersRole? = user.roles?.find { urf ->
                        urf.roleId == it.roleId
                    }
                    if (it.status == REQUEST_ROLE_STATUS_REMOVE) {
                        if (findUserRole == null) {
                            listMessage.add(
                                ErrorMessage(
                                    "roles", "User tidak mempunya role ${check.get().name}"
                                )
                            )
                        }
                    } else {
                        if (findUserRole != null) {
                            listMessage.add(
                                ErrorMessage(
                                    "roles", "User sudah mempunyai ${check.get().name}"
                                )
                            )
                        }
                    }
                }
            }

            if (!request.profilePictureId.isNullOrEmpty()) {
                val chekFile: ReturnData = RequestHelpers.filesGetById(
                    request.profilePictureId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )
                if (!chekFile.success!!) {
                    listMessage.add(ErrorMessage("profilePictureId", chekFile.message))
                } else {
                    rData["files"] = chekFile.data as Files
                }
            }

            if (!request.componentId.isNullOrEmpty()) {
                val checkComponent = repoComponent.findByIdAndActive(request.componentId)
                if (!checkComponent.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "componentId", "Id Komponent ${request.componentId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    rData["component"] = checkComponent.get()
                }
            }

            if (!request.provinceId.isNullOrEmpty()) {
                val checkProvince = repoProvince.findByIdAndActive(request.provinceId)
                if (!checkProvince.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "provinceId", "Id Proince ${request.provinceId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    rData["province"] = checkProvince.get()
                }
            }

            val newRole: MutableList<Role> = mutableListOf()
            val allRole = repoRole.getAllByActive()
            val roleUser: MutableSet<UsersRole> = user.roles ?: emptyList<UsersRole>().toMutableSet()

            roleUser.forEach { ru ->
                ru.role?.let { newRole.add(it) }
            }

            request.roles?.forEach { rr ->
                if (rr.status == REQUEST_ROLE_STATUS_REMOVE) {
                    newRole.remove(newRole.find { nrf -> nrf.id == rr.roleId })
                } else {
                    allRole.find { nrf -> nrf.id == rr.roleId }?.let { newRole.add(it) }
                }
            }

            rData["roles"] = allRole
            listMessage.addAll(validateRole(newRole))

            val filterLsp = newRole.filter {
                it.id == ROLE_ID_LSP
            }

            if (filterLsp.isNotEmpty()) {
                var lspStartDate: Date? = null
                var lspEndDate: Date? = null

                val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

                if (request.lspStartDate.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "lspStartDate", "Tanggal Mulai LSP $VALIDATOR_MSG_REQUIRED"
                        )
                    )
                } else {
                    try {
                        lspStartDate = dform.parse(request.lspStartDate)
                    } catch (_: Exception) {
                        listMessage.add(
                            ErrorMessage(
                                "lspExpiredDate", "Tanggal Mulai LSP $VALIDATOR_MSG_NOT_VALID"
                            )
                        )
                    }
                }

                if (request.lspEndDate.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "lspEndDate", "Tanggal Akhir LSP $VALIDATOR_MSG_REQUIRED"
                        )
                    )
                } else {
                    try {
                        lspEndDate = dform.parse(request.lspEndDate)
                    } catch (_: Exception) {
                        listMessage.add(
                            ErrorMessage(
                                "lspEndDate", "Tanggal Akhir LSP $VALIDATOR_MSG_NOT_VALID"
                            )
                        )
                    }
                }

                if (lspStartDate != null) {
                    rData["lspStartDate"] = lspStartDate
                }

                if (lspEndDate != null) {
                    if (lspEndDate.before(lspStartDate)) {
                        listMessage.add(
                            ErrorMessage(
                                "lspEndDate", "Tanggal Akhir LSP $VALIDATOR_MSG_NOT_VALID"
                            )
                        )
                    }

                    rData["lspEndDate"] = lspEndDate
                }

                val awpImplementations = mutableListOf<AwpImplementation>()
                request.lspAwpImplementationIds?.forEach {
                    val checkAwp = repoAwpImplementation.findByIdAndActive(it)
                    if (!checkAwp.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "lspAwpImplementationId", "Awp Implementation id $it $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    } else {
                        awpImplementations.add(checkAwp.get())
                    }
                }
                rData["awpImplementation"] = awpImplementations
            }

            val inputRoleIds = request.roles?.filter { rf ->
                rf.roleId == ROLE_ID_PCU
            }

            if (inputRoleIds?.isNotEmpty() == true) {
                val userIds = repoUserRoleUser.getUserIdsByRoleIds(listOf(ROLE_ID_PCU), 1)
                if (userIds.isNotEmpty()) {
                    val usersPcu = repo.getByIdInAndProvinceIdAndActive(userIds, request.provinceId)
                    if (usersPcu.isNotEmpty()) {
                        if (usersPcu[0].id != user.id) {
                            listMessage.add(
                                ErrorMessage(
                                    "roles", "User PCU Provinsi ${usersPcu[0].province?.name} $VALIDATOR_MSG_HAS_ADDED"
                                )
                            )
                        }

                    }
                }
            }

            rData["listMessage"] = listMessage
        } catch (_: Exception) {

        }

        return rData
    }

    fun validateRole(selectedRole: List<Role>): List<ErrorMessage> {
        val listMessage = mutableListOf<ErrorMessage>()
        try {
            val roleHasSupervision = selectedRole.filter {
                it.supervisiorId != null && it.supervisiorId != ""
            }

            val dupicateSUpervisior = mutableListOf<String>()

            selectedRole.forEach {
                if (!it.supervisiorId.isNullOrEmpty()) { // check role supervisi berbeda
                    if (roleHasSupervision.isNotEmpty()) {
                        val findDiffRole = roleHasSupervision.filter { f ->
                            f.supervisiorId != it.supervisiorId
                        }

                        if (findDiffRole.isNotEmpty()) {
                            var diffRoleName = ""
                            findDiffRole.forEach { fd ->
                                if (!dupicateSUpervisior.contains(fd.supervisiorId)) {
                                    dupicateSUpervisior.add(fd.supervisiorId ?: "")
                                }

                                if (diffRoleName != "") {
                                    diffRoleName += ", "
                                }
                                diffRoleName += fd.name
                            }

                            if (!dupicateSUpervisior.contains(it.supervisiorId)) {
                                listMessage.add(
                                    ErrorMessage(
                                        "roles",
                                        "Role ${it.name} memiliki supervisi yang berbeda dengan role $diffRoleName"
                                    )
                                )
                            }
                        }
                    }
                } else {
                    if (roleHasSupervision.isNotEmpty()) { // check role yang di supervisi
                        val findRoleBySupervision = roleHasSupervision.filter { f ->
                            it.id == f.supervisiorId
                        }

                        if (findRoleBySupervision.isNotEmpty()) {
                            var roleBySupervisionName = ""
                            findRoleBySupervision.forEach { fr ->
                                if (roleBySupervisionName != "") {
                                    roleBySupervisionName += ", "
                                }
                                roleBySupervisionName += fr.name
                            }

                            listMessage.add(
                                ErrorMessage(
                                    "roles", "Role ${it.name} tidak dapat memiliki role $roleBySupervisionName"
                                )
                            )
                        } else { // chek semua child role
                            supervisiorChild.clear()
                            getChild(repoRole.getAllByActive(), it)
                            val roleBySuperVisior: List<Role> = supervisiorChild.filter { ra ->
                                (!ra.supervisiorId.isNullOrEmpty() && ra.supervisiorId == it.id)
                            }.filter { f2 ->
                                val f = selectedRole.filter { f3 ->
                                    f3.supervisiorId == f2.id
                                }
                                f.isNotEmpty()
                            }

                            if (roleBySuperVisior.isNotEmpty()) {
                                var roleBySupervisionName = ""
                                roleBySuperVisior.forEach { fr ->
                                    val selected = selectedRole.find { sf ->
                                        sf.supervisiorId == fr.id
                                    }

                                    if (roleBySupervisionName != "") {
                                        roleBySupervisionName += ", "
                                    }
                                    roleBySupervisionName += selected?.name
                                }

                                listMessage.add(
                                    ErrorMessage(
                                        "roles", "Role ${it.name} tidak dapat memiliki role $roleBySupervisionName"
                                    )
                                )
                            }
                        }
                    }
                }
            }
        } catch (_: Exception) {

        }
        return listMessage
    }

    private fun getChild(allRole: List<Role>, role: Role) {
        try {
            val find = allRole.filter {
                it.supervisiorId == role.id
            }

            if (find.isNotEmpty()) {
                supervisiorChild.addAll(find)

                find.filter { ff ->
                    ff.id != role.id
                }.forEach { r ->
                    allRole.find { it.supervisiorId == r.id }?.let { s ->
                        val parrent = allRole.find { alf ->
                            alf.id == s.supervisiorId
                        }
                        if (parrent != null) {
                            getChild(allRole, parrent)
                        }
                    }
                }
            }
        } catch (_: Exception) {

        }
    }
}
