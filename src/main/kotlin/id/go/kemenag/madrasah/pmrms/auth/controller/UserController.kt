package id.go.kemenag.madrasah.pmrms.auth.controller

import id.go.kemenag.madrasah.pmrms.auth.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.auth.model.request.user.UserSaveRequest
import id.go.kemenag.madrasah.pmrms.auth.model.request.user.UserUpdateRequest
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.service.UserService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["User"], description = "User API")
@RestController
@RequestMapping(path = ["user"])
class UserController {

    @Autowired
    private lateinit var service: UserService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(value = ["datatable-lsp"], produces = ["application/json"])
    fun datatableLsp(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableLsp(req)
    }

    @PostMapping(produces = ["application/json"])
    fun save(@Valid @RequestBody request: UserSaveRequest): ResponseEntity<ReturnData> {
        return service.save(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(
        @PathVariable id: String,
        @Valid @RequestBody request: UserUpdateRequest
    ): ResponseEntity<ReturnData>? {
        return service.update(id, request)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }
}
