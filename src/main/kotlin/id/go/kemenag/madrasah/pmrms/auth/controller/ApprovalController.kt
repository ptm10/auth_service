package id.go.kemenag.madrasah.pmrms.auth.controller

import id.go.kemenag.madrasah.pmrms.auth.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.auth.model.request.approval.ApproveRequest
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.service.ApprovalService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@Api(tags = ["Approval"], description = "Approval API")
@RestController
@RequestMapping(path = ["approval"])
class ApprovalController {

    @Autowired
    private lateinit var service: ApprovalService

    @PostMapping(value = [""], produces = ["application/json"])
    fun approve(@Valid @RequestBody request: ApproveRequest): ResponseEntity<ReturnData> {
        return service.approve(request)
    }

    @GetMapping(value = ["pending-update-role-request"], produces = ["application/json"])
    fun pendingUpdateRoleRequest(): ResponseEntity<ReturnData> {
        return service.pendingUpdateRoleRequest()
    }

    @PostMapping(value = ["role-request-list"], produces = ["application/json"])
    fun roleRequestList(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.roleRequestList(req)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }
}
