package id.go.kemenag.madrasah.pmrms.auth.controller

import id.go.kemenag.madrasah.pmrms.auth.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.service.RoleService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Role"], description = "Role API")
@RestController
@RequestMapping(path = ["role"])
class RoleController {

    @Autowired
    private lateinit var service: RoleService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }
}
