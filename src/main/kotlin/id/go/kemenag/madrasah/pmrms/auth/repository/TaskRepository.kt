package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.Task
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TaskRepository : JpaRepository<Task, String> {

    fun findByTaskIdAndTaskTypeAndActive(taskId: String?, taskType: Int?, active: Boolean? = true) : Optional<Task>
}
