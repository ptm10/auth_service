package id.go.kemenag.madrasah.pmrms.auth.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "awp_implementation", schema = "public")
data class AwpImplementation(

    @Id @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "lsp_pic")
    var lspPic: String? = null,

    @Column(name = "component_id")
    var componentId: String? = null,

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = null
)
