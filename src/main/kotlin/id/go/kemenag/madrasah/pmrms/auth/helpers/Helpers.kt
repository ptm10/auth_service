@file:Suppress("UNCHECKED_CAST")

package id.go.kemenag.madrasah.pmrms.auth.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.auth.constant.PASSWORD_REGEX
import id.go.kemenag.madrasah.pmrms.auth.pojo.UsersRole
import id.go.kemenag.madrasah.pmrms.auth.pojo.VUsersResources
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.security.core.context.SecurityContextHolder
import java.net.MalformedURLException
import java.net.URL
import java.util.*
import javax.servlet.http.HttpServletRequest

fun getUserLogin(): VUsersResources? {
    return try {
        val principal = SecurityContextHolder.getContext().authentication.principal as Any
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        objectMapper.readValue(principal.toString(), VUsersResources::class.java)
    } catch (_: Exception) {
        null
    }
}

fun getUserRoles(): MutableSet<UsersRole> {
    var roles: MutableSet<UsersRole> = mutableSetOf()
    try {
        getUserLogin()?.let {
            if (!it.roles.isNullOrEmpty()) {
                roles = getUserLogin()!!.roles!!
            }
        }
    } catch (_: Exception) {
    }

    return roles
}

fun userHasRoles(roles: List<String>): Boolean {
    var allowed = false
    try {
        roles.forEach {
            val findRole: UsersRole? = getUserRoles().find { f ->
                f.roleId == it
            }
            if (findRole != null) {
                allowed = true
                return@forEach
            }
        }
    } catch (_: Exception) {
    }
    return allowed
}

@Throws(MalformedURLException::class)
fun getBaseUrl(request: HttpServletRequest): String {
    try {
        val requestURL = URL(request.requestURL.toString())
        val port = if (requestURL.port == -1) "" else ":" + requestURL.port
        return requestURL.protocol + "://" + requestURL.host + port + request.contextPath + "/"
    } catch (e: Exception) {
        throw e
    }
}

fun getFullUrl(request: HttpServletRequest): String {
    val requestURL = StringBuilder(request.requestURL.toString())
    val queryString = request.queryString
    return if (queryString == null) {
        requestURL.toString()
    } else {
        requestURL.append('?').append(queryString).toString()
    }
}

fun randomPassword(): String {
    val minimum = 8
    val maximum = 12
    val rn = Random()
    val range: Int = maximum - minimum + 1
    val length: Int = rn.nextInt(range) + minimum

    var chars = ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi"
            + "jklmnopqrstuvwxyz")
    val spec = "@#$%^&+="
    chars += RandomStringUtils.random(2, spec)

    val rnd = Random()
    val sb = StringBuilder(length)
    for (i in 0 until length) sb.append(chars[rnd.nextInt(chars.length)])

    return sb.toString()
}

fun generateRandomPassword(): String {
    var password = ""
    while (password == "") {
        val random = randomPassword()
        if (random.matches(Regex(PASSWORD_REGEX))) {
            password = random
        }
    }
    return password
}

fun setCompareData(strData: String): Map<String, Any?> {
    val setData = mutableMapOf<String, Any?>()
    val objectMapper = ObjectMapper()

    try {
        val map: Map<String, Any?> = objectMapper.readValue(strData, Map::class.java) as Map<String, Any?>

        val ignoreField = listOf("createdAt", "updatedAt", "createdBy", "createdByUser", "updatedBy", "updatedByUser")

        map.forEach {
            if (!ignoreField.contains(it.key)) {
                when (it.value) {
                    is String -> {
                        setData[it.key] = it.value.toString().trim().lowercase()
                    }
                    is Int -> {
                        setData[it.key] = it.value
                    }
                    is Long -> {
                        setData[it.key] = it.value
                    }
                    is Double -> {
                        setData[it.key] = it.value
                    }
                    is Date -> {
                        setData[it.key] = it.value
                    }
                    is Boolean -> {
                        setData[it.key] = it.value
                    }

                    else -> {
                        try {
                            val parse = objectMapper.writeValueAsString(it.value)
                            if (parse[0] == '[') {
                                try {
                                    val mapValue = it.value as List<Map<String, Any?>>
                                    val mapDatas = mutableListOf<Map<String, Any?>>()
                                    mapValue.forEach { mv ->
                                        mapDatas.add(
                                            setCompareData(
                                                objectMapper.writeValueAsString(mv)
                                            )
                                        )
                                    }
                                    setData[it.key] = mapDatas
                                } catch (_: Exception) {

                                }
                            }
                        } catch (_: Exception) {

                        }
                    }
                }
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    
    return setData
}
