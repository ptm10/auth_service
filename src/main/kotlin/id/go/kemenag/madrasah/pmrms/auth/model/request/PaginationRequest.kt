package id.go.kemenag.madrasah.pmrms.auth.model.request

data class PaginationRequest(

    var enablePage: Int? = 1,

    var page: Int? = 0,

    var size: Int? = 10,

    var sort: String? = "desc",

    var sortBy: String? = "updatedAt",

    var param: String? = ""

)
