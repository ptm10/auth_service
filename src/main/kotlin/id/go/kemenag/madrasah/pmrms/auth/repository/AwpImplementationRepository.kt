package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.AwpImplementation
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface AwpImplementationRepository : JpaRepository<AwpImplementation, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<AwpImplementation>

    fun getByLspPicAndComponentIdAndActive(
        lspPic: String?,
        componentId: String?,
        active: Boolean = true
    ): List<AwpImplementation>
}
