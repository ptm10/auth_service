package id.go.kemenag.madrasah.pmrms.auth.constant

const val ROLE_ID_ADMINSTRATOR = "efff3828-105c-11ec-920c-dbd208503209"

const val ROLE_ID_LSP = "c266ece8-991a-11ec-a3bb-06ce2a1d1852"

const val ROLE_ID_PCU = "59a8c0c2-ec86-11ec-88a7-270198da8227"

val ROLES_CHANGE_ROLE_NOTIF = listOf(ROLE_ID_ADMINSTRATOR)
