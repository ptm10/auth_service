package id.go.kemenag.madrasah.pmrms.auth.model.response

data class ReturnData(
    var success: Boolean? = false,
    var data: Any? = null,
    var message: String? = null
)

