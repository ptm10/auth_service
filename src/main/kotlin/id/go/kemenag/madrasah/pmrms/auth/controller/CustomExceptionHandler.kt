package id.go.kemenag.madrasah.pmrms.auth.controller

import id.go.kemenag.madrasah.pmrms.auth.exception.BaseException
import id.go.kemenag.madrasah.pmrms.auth.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.InvalidDataAccessApiUsageException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindException
import org.springframework.validation.BindingResult
import org.springframework.validation.ObjectError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.function.Consumer


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
class CustomExceptionHandler : ResponseEntityExceptionHandler() {

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        val errors = mutableListOf<ErrorMessage>()
        ex.bindingResult.fieldErrors.forEach { fieldError ->
            errors.add(ErrorMessage(fieldError.field, fieldError.defaultMessage.toString()))
        }

        generateObjectErrors(ex.bindingResult)?.let {
            errors.add(ErrorMessage(it.keys.toString(), it.values.toString()))
        }
        ex.printStackTrace()

        return ResponseEntity<Any>(
            ReturnData(
                data = errors,
                message = status.name
            ),
            status
        )
    }

    private fun generateObjectErrors(result: BindingResult): Map<String, String>? {
        val fieldErrorMap: MutableMap<String, String> = HashMap()
        result.globalErrors.forEach(Consumer { objectError: ObjectError ->
            fieldErrorMap["confirmPassword"] = objectError.defaultMessage ?: ""
        })

        if (fieldErrorMap.isNotEmpty()) {
            return fieldErrorMap
        }
        return null
    }

    override fun handleBindException(
        ex: BindException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        val errors = mutableListOf<ErrorMessage>()
        ex.bindingResult.fieldErrors.forEach { fieldError ->
            errors.add(ErrorMessage(fieldError.field, fieldError.defaultMessage.toString()))
        }

        generateObjectErrors(ex.bindingResult)?.let {
            errors.add(ErrorMessage(it.keys.toString(), it.values.toString()))
        }
        ex.printStackTrace()

        return ResponseEntity<Any>(
            ReturnData(
                data = errors,
                message = status.name
            ),
            status
        )
    }

    @ExceptionHandler(RuntimeException::class)
    fun handleRuntimeException(ex: Exception, request: WebRequest?): ResponseEntity<Any?>? {
        ex.printStackTrace()
        val response = ReturnData()
        response.success = false
        response.message = "Terjadi kesalahan sistem. Silahkan ulangi beberapa saat lagi"

        /*if (springProfileActive == "development") {
            response.message = ex.message
        }*/

        var status = HttpStatus.INTERNAL_SERVER_ERROR

        if (ex is BaseException) {
            ex.printStackTrace()
            if (ex.status != status) {
                response.message = ex.message

                ex.data?.let {
                    response.data = it
                }

                status = ex.status
            }
        } else {
            return ResponseEntity(response, status)
        }
        return ResponseEntity(response, status)
    }

    @ExceptionHandler(DataIntegrityViolationException::class)
    fun handleDataIntegrityViolationException(ex: Exception, request: WebRequest?): ResponseEntity<Any?>? {
        ex.printStackTrace()
        val response = ReturnData()
        response.success = false
        response.message = "Bad Request"

        var status = HttpStatus.BAD_REQUEST

        if (ex is BaseException) {
            ex.printStackTrace()
            if (ex.status != status) {
                response.message = ex.message

                ex.data?.let {
                    response.data = it
                }

                status = ex.status
            }
        } else {
            return ResponseEntity(response, status)
        }
        return ResponseEntity(response, status)
    }

    @ExceptionHandler(InvalidDataAccessApiUsageException::class)
    fun handleInvalidDataAccessApiUsageException(ex: Exception, request: WebRequest?): ResponseEntity<Any?>? {
        ex.printStackTrace()
        val response = ReturnData()
        response.success = false
        response.message = "Bad Request"

        var status = HttpStatus.BAD_REQUEST

        if (ex is BaseException) {
            ex.printStackTrace()
            if (ex.status != status) {
                response.message = ex.message

                ex.data?.let {
                    response.data = it
                }

                status = ex.status
            }
        } else {
            return ResponseEntity(response, status)
        }
        return ResponseEntity(response, status)
    }

    @ExceptionHandler(NoSuchElementException::class)
    fun handleNoSuchElementException(ex: Exception, request: WebRequest?): ResponseEntity<Any?>? {
        ex.printStackTrace()
        val response = ReturnData()
        response.success = false
        response.message = ex.message

        var status = HttpStatus.NOT_FOUND

        if (ex is BaseException) {
            ex.printStackTrace()
            if (ex.status != status) {
                response.message = ex.message

                ex.data?.let {
                    response.data = it
                }

                status = ex.status
            }
        } else {
            return ResponseEntity(response, status)
        }
        return ResponseEntity(response, status)
    }

    @ExceptionHandler(IllegalArgumentException::class)
    fun handleIllegalArgumentException(ex: Exception, request: WebRequest?): ResponseEntity<Any?>? {
        ex.printStackTrace()
        val response = ReturnData()
        response.success = false
        response.message = ex.message

        var status = HttpStatus.BAD_REQUEST

        if (ex is BaseException) {
            ex.printStackTrace()
            if (ex.status != status) {
                response.message = ex.message

                ex.data?.let {
                    response.data = it
                }

                status = ex.status
            }
        } else {
            return ResponseEntity(response, status)
        }
        return ResponseEntity(response, status)
    }
}
