package id.go.kemenag.madrasah.pmrms.auth.controller

import id.go.kemenag.madrasah.pmrms.auth.helpers.RequestHelpers
import id.go.kemenag.madrasah.pmrms.auth.model.request.PaginationRequest
import id.go.kemenag.madrasah.pmrms.auth.model.request.auth.*
import id.go.kemenag.madrasah.pmrms.auth.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.auth.service.AuthService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Auth"], description = "Auth API")
@RestController
@RequestMapping(path = ["auth"])
class AuthController {

    @Value("\${tableau.token.url}")
    private lateinit var tableauTokenUrl: String

    @Autowired
    private lateinit var service: AuthService

    @PostMapping(value = ["login"], produces = ["application/json"])
    fun login(@Valid @RequestBody request: LoginRequest): ResponseEntity<ReturnData> {
        return service.login(request)
    }

    @PostMapping(value = ["forgot-password"], produces = ["application/json"])
    fun forgotPassword(@Valid @RequestBody request: ForgotPasswordRequest): ResponseEntity<ReturnData> {
        return service.forgotPassword(request)
    }

    @GetMapping(value = ["forgot-password/{id}"], produces = ["application/json"])
    fun forgotPasswordValidate(@PathVariable("id") id: String): ResponseEntity<ReturnData> {
        return service.forgotPasswordValidate(id)
    }

    @GetMapping(value = ["detail"], produces = ["application/json"])
    fun detail(): ResponseEntity<ReturnData> {
        return service.detail()
    }

    @PutMapping(value = ["reset-password"], produces = ["application/json"])
    fun resetPassword(@Valid @RequestBody request: ResetPasswordRequest): ResponseEntity<ReturnData> {
        return service.resetPassword(request)
    }

    @PutMapping(value = ["update-profile"], produces = ["application/json"])
    fun updateProfile(@Valid @RequestBody request: UpdateProfileRequest): ResponseEntity<ReturnData> {
        return service.updateProfile(request)
    }

    @PutMapping(value = ["update-password"], produces = ["application/json"])
    fun updatePassword(@Valid @RequestBody request: UpdatePasswordRequest): ResponseEntity<ReturnData> {
        return service.updatePassword(request)
    }

    @GetMapping(value = ["roles"], produces = ["application/json"])
    fun roles(@Valid @ModelAttribute page: PaginationRequest): ResponseEntity<ReturnData> {
        return service.roles(page)
    }

    @GetMapping(value = ["required-change-password"], produces = ["application/json"])
    fun requiredChangePassword(): ResponseEntity<ReturnData> {
        return service.requiredChangePassword()
    }

    @DeleteMapping(value = ["cancel-update-role-request"], produces = ["application/json"])
    fun cancelUpdateRoleRequest(): ResponseEntity<ReturnData> {
        return service.cancelUpdateRoleRequest()
    }

    @DeleteMapping(value = ["cancel-update-email"], produces = ["application/json"])
    fun cancelUpdateEmail(): ResponseEntity<ReturnData> {
        return service.cancelUpdateEmail()
    }

    @PutMapping(value = ["change-email"], produces = ["application/json"])
    fun changeEmail(@Valid @RequestBody request: ChangeEmailRequestRequestRequest): ResponseEntity<ReturnData> {
        return service.changeEmail(request)
    }

    @PutMapping(value = ["confirm-change-email"], produces = ["application/json"])
    fun confirmChangeEmail(@Valid @RequestBody request: ConfirmChangeEmailRequest): ResponseEntity<ReturnData> {
        return service.confirmChangeEmail(request)
    }

    @GetMapping(value = ["change-email-request"], produces = ["application/json"])
    fun changeEmailRequest(): ResponseEntity<ReturnData> {
        return service.changeEmailRequest()
    }

    @GetMapping(value = ["get-change-email-by-id-identity"], produces = ["application/json"])
    fun getChangeEmailByIdentity(
        @RequestParam("id") id: String?,
        @RequestParam("identity") identityId: String?
    ): ResponseEntity<ReturnData> {
        return service.getChangeEmailByIdentity(id, identityId)
    }

    @GetMapping("tableau-token")
    fun getTableauToken() : String? {
        return RequestHelpers.requestTableauToken(tableauTokenUrl)
    }
}
