package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.Province
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ProvinceRepository : JpaRepository<Province, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<Province>
}
