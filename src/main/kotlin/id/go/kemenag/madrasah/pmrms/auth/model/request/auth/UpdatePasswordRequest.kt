package id.go.kemenag.madrasah.pmrms.auth.model.request.auth


import id.go.kemenag.madrasah.pmrms.auth.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class UpdatePasswordRequest(

    @field:NotEmpty(message = "Password Lama $VALIDATOR_MSG_REQUIRED")
    var oldPassword: String? = null,

    @field:NotEmpty(message = "Password Baru $VALIDATOR_MSG_REQUIRED")
    var newPassword: String? = null,

    @field:NotEmpty(message = "Password Konfirmasi $VALIDATOR_MSG_REQUIRED")
    var confirmPassword: String? = null
)
