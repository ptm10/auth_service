package id.go.kemenag.madrasah.pmrms.auth.repository


import id.go.kemenag.madrasah.pmrms.auth.pojo.UsersForgotPassword
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserForgotPasswordRepository : JpaRepository<UsersForgotPassword, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<UsersForgotPassword>
}
