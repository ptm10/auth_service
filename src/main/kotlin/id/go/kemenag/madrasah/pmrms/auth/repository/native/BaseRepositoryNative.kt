package id.go.kemenag.madrasah.pmrms.auth.repository.native

import id.go.kemenag.madrasah.pmrms.auth.model.request.Pagination2Request
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.support.PageableExecutionUtils
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager
import javax.persistence.Query

@Suppress("UNCHECKED_CAST")
open class BaseRepositoryNative<T>(_class: Class<T>?) {

    private var activeData: Boolean = true

    private val collection = _class?.simpleName

    @Autowired
    private lateinit var em: EntityManager

    open fun setActiveData(activeParam: Boolean) {
        activeData = activeParam
    }

    @Transactional
    open fun getPage(req: Pagination2Request): Page<T>? {
        try {
            var qActive = "s.active = true"
            if (!this.activeData) {
                qActive = "s.active IN(true, false)"
            }

            var queryStr = "SELECT s FROM $collection s WHERE $qActive"

            if (!req.paramLike.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramLike?.forEachIndexed { index, element ->
                    queryStr += "LOWER(s.${element.field}) LIKE LOWER(CONCAT('%',:${replaceParamQuery(element.field ?: "")},'%'))"

                    if ((index + 1) < req.paramLike!!.size) {
                        queryStr += " OR "
                    }
                }

                queryStr += ")"
            }

            if (!req.paramIs.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramIs?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} = :${replaceParamQuery(element.field ?: "")}"

                    if ((index + 1) < req.paramIs!!.size) {
                        queryStr += " AND "
                    }
                }

                queryStr += ")"
            }

            if (!req.paramIn.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramIn?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} IN(:${replaceParamQuery(element.field ?: "")})"

                    if ((index + 1) < req.paramIs!!.size) {
                        queryStr += " AND "
                    }
                }

                queryStr += ")"
            }

            if (!req.paramNotIn.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramNotIn?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} NOT IN(:${replaceParamQuery(element.field ?: "")})"

                    if ((index + 1) < req.paramIs!!.size) {
                        queryStr += " AND "
                    }
                }

                queryStr += ")"
            }

            if (!req.sort.isNullOrEmpty()) {
                queryStr += " ORDER BY "

                req.sort?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} ${element.direction}"

                    if ((index + 1) < req.sort!!.size) {
                        queryStr += ", "
                    }
                }
            }

            val query: Query = em.createQuery(queryStr, Any::class.java)

            req.paramLike?.forEach {
                query.setParameter(replaceParamQuery(it.field ?: ""), it.value)
            }

            req.paramIs?.forEach {
                if (it.dataType == "int") {
                    query.setParameter(replaceParamQuery(it.field ?: ""), Integer.valueOf(it.value))
                } else {
                    query.setParameter(replaceParamQuery(it.field ?: ""), it.value)
                }
            }

            req.paramIn?.forEach {
                query.setParameter(replaceParamQuery(it.field ?: ""), it.value)
            }

            req.paramNotIn?.forEach {
                query.setParameter(replaceParamQuery(it.field ?: ""), it.value)
            }

            val size: Long = query.resultList.size.toLong()

            if (req.enablePage == true) {
                query.firstResult = req.page!! * req.size!!
                query.maxResults = req.size!!
            }

            val list: List<T> = query.resultList as List<T>

            var pageable: Pageable = PageRequest.of(req.page!!, req.size!!)
            if (req.enablePage != true) {
                pageable = Pageable.unpaged()
            }

            return PageableExecutionUtils.getPage(
                list,
                pageable
            ) {
                size
            }
        } catch (e: Exception) {
            throw e
        }
    }
}

private fun replaceParamQuery(param: String): String {
    return param.replace(".", "_")
}
