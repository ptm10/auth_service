package id.go.kemenag.madrasah.pmrms.auth.repository.native

import id.go.kemenag.madrasah.pmrms.auth.pojo.Position
import org.springframework.stereotype.Repository

@Repository
class PositionRepositoryNative : BaseRepositoryNative<Position>(Position::class.java)
