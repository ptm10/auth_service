package id.go.kemenag.madrasah.pmrms.auth.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.auth.constant.VALIDATOR_MSG_REQUIRED
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = "position", schema = "auth")
data class Position(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "unit_id")
    var unitId: String? = null,

    @ManyToOne
    @JoinColumn(name = "unit_id", insertable = false, updatable = false, nullable = true)
    @Where(clause = "active = true")
    var unit: Unit? = null,

    @Column(name = "name")
    @field:NotEmpty(message = "Nama $VALIDATOR_MSG_REQUIRED")
    var name: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
