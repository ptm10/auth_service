package id.go.kemenag.madrasah.pmrms.auth.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.auth.constant.APPROVAL_STATUS_REQUEST
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "update_role_request", schema = "auth")
data class UpdateRoleRequest(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "user_id")
    var userId: String? = null,

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false, nullable = true)
    var user: Users? = null,

    @Column(name = "approve_status")
    var approveStatus: Int? = APPROVAL_STATUS_REQUEST,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "updated_by", insertable = false, updatable = false, nullable = true)
    var updatedByUser: Users? = null,

    @OneToMany(mappedBy = "updateRoleRequestId")
    var detail: MutableList<UpdateRoleRequestDetail>? = emptyList<UpdateRoleRequestDetail>().toMutableList(),

    @Column(name = "rejected_message")
    var rejectedMessage: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
