package id.go.kemenag.madrasah.pmrms.auth.repository.native

import id.go.kemenag.madrasah.pmrms.auth.pojo.UpdateRoleRequest
import org.springframework.stereotype.Repository

@Repository
class UpdateRoleRequestRepositoryNative : BaseRepositoryNative<UpdateRoleRequest>(UpdateRoleRequest::class.java)
