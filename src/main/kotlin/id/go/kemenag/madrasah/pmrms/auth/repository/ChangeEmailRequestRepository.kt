package id.go.kemenag.madrasah.pmrms.auth.repository

import id.go.kemenag.madrasah.pmrms.auth.pojo.ChangeEmailRequest
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ChangeEmailRequestRepository : JpaRepository<ChangeEmailRequest, String> {

    fun findByIdAndActive(id: String? = null, active: Boolean? = true): Optional<ChangeEmailRequest>

    fun getByUserIdAndActive(userId: String? = null, active: Boolean? = true): List<ChangeEmailRequest>
}
