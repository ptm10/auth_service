package id.go.kemenag.madrasah.pmrms.auth.model.request.auth


import id.go.kemenag.madrasah.pmrms.auth.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class ConfirmChangeEmailRequest(

    @field:NotEmpty(message = "Id Request $VALIDATOR_MSG_REQUIRED")
    var requestId: String? = null,

    @field:NotNull(message = "Status $VALIDATOR_MSG_REQUIRED")
    var status: Int? = null,

    @field:NotNull(message = "Confirmation Identity $VALIDATOR_MSG_REQUIRED")
    var confirmationIdentity: String? = null,
)
