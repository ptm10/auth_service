package id.go.kemenag.madrasah.pmrms.auth.model.request.auth

import id.go.kemenag.madrasah.pmrms.auth.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty


class ResetPasswordRequest(
    @field:NotEmpty(message = "Request id $VALIDATOR_MSG_REQUIRED")
    var requestId: String? = null,

    @field:NotEmpty(message = "Password $VALIDATOR_MSG_REQUIRED")
    var password: String? = null,

    @field:NotEmpty(message = "Konfirmasi Password $VALIDATOR_MSG_REQUIRED")
    var confirmPassword: String? = null
)
